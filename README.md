UMaxIt
=======
--------------------------------------------------
![Alt text](/readme/umaxit-logo.png)
--------------------------------------------------

Description
--------------

UMaxIt is my first and only game that I implemented when I was learning iOS development. It was done a few years back, basically it is a implementation of the "Max It" pc game from the 80's.

It is written in Objective-C and uses some of features from the GameKit framework. I have updated it to support Game Center automatch feature (as in version 2.0).

App Store: [Link](https://apps.apple.com/us/app/umaxit/id635594621)

-------------

![Alt text](/readme/umaxit.gif)

-------------
