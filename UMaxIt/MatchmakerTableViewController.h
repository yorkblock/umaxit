//
//  Copyright (c) 2020 York Block. All rights reserved.
//


#import <UIKit/UIKit.h>
#import <GameKit/GameKit.h>


@class MatchmakerTableViewController;

@protocol MatchmakerTableViewControllerDelegate <NSObject>

- (void)turnBasedMatchmakerTableViewControllerWasCancelled:(MatchmakerTableViewController *)viewController;
- (void)turnBasedMatchmakerTableViewController:(MatchmakerTableViewController *)viewController didFailWithError:(NSError *)error;
- (void)turnBasedMatchmakerTableViewController:(MatchmakerTableViewController *)viewController didLoadMatch:(GKTurnBasedMatch *)match;

@end


@interface MatchmakerTableViewController : UITableViewController

@property (nonatomic, weak) id<MatchmakerTableViewControllerDelegate>  delegate;

@end
