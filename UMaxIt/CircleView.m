//
//  Copyright (c) 2015 York Block. All rights reserved.
//

#import "CircleView.h"


@implementation CircleView

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [[self layer] setCornerRadius:[self bounds].size.height / 2.0f];
    [[self layer] setBorderColor:[[UIColor clearColor] CGColor]];
    [[self layer] setMasksToBounds:YES];
}

@end
