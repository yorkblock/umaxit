//
//  Copyright (c) 2013 York Block. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UMaxIt.h"


@interface Settings : NSObject

@property (nonatomic) BOOL    isSoundOn;
@property (nonatomic) UMTheme theme;

- (void)save;
- (void)load;
+ (BOOL)isFirstLaunch;

@end
