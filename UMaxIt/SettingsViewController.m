//
//  Copyright (c) 2013 York Block. All rights reserved.
//

#import "SettingsViewController.h"
#import "DDLog.h"


static const int ddLogLevel = LOG_LEVEL_ERROR;


@interface SettingsViewController ()

@property (nonatomic, weak) IBOutlet UIButton *closeButton;
@property (nonatomic, weak) IBOutlet UILabel  *themeLabel;
@property (nonatomic, weak) IBOutlet UILabel  *uMaxItLabel;
@property (nonatomic, weak) IBOutlet UILabel  *funLabel;
@property (nonatomic, weak) IBOutlet UILabel  *ecoLabel;
@property (nonatomic, weak) IBOutlet UILabel  *soundLabel;
@property (nonatomic, weak) IBOutlet UIButton *soundButton;

@property (nonatomic, strong) Settings *settings;

@end

@implementation SettingsViewController

- (IBAction)close:(id)sender
{
    [[self presentingViewController] dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)selectUMaxItTheme:(id)sender
{
    [UIView animateWithDuration:0.5 animations:^{
        [[self uMaxItLabel] setAlpha:1.0f];
        [[self funLabel] setAlpha:0.0f];
        [[self ecoLabel] setAlpha:0.0f];

        [[self settings] setTheme:UMThemeClassic];
        [[self settings] save];
    }];
}

- (IBAction)selectFunTheme:(id)sender
{
    [UIView animateWithDuration:0.5 animations:^{
        [[self uMaxItLabel] setAlpha:0.0f];
        [[self funLabel] setAlpha:1.0f];
        [[self ecoLabel] setAlpha:0.0f];
        
        [[self settings] setTheme:UMThemeModern];
        [[self settings] save];
    }];
}

- (IBAction)selectEcoTheme:(id)sender
{
    [UIView animateWithDuration:0.5 animations:^{
        [[self uMaxItLabel] setAlpha:0.0f];
        [[self funLabel] setAlpha:0.0f];
        [[self ecoLabel] setAlpha:1.0f];

        [[self settings] setTheme:UMThemeEco];
        [[self settings] save];
    }];
}

// ◉ ◎ ✔

- (IBAction)enableSound:(id)sender
{
    DDLogVerbose(@"Enable (%d)", [sender isSelected]);
    [sender setSelected:![sender isSelected]];
    
    [[self settings] setIsSoundOn:![sender isSelected]];
    [[self settings] save];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [[self themeLabel] setText:[NSString stringWithFormat:@"%@", NSLocalizedString(@"THEMES", nil)]];
    [[self soundLabel] setText:[NSString stringWithFormat:@"%@", NSLocalizedString(@"ENABLED SOUND", nil)]];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 13.0)
        [[self closeButton] setHidden:NO];
    
    [self setSettings:[[Settings alloc] init]];
    [[self settings] load];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[self soundButton] setSelected:![[self settings] isSoundOn]];
    
    switch ([[self settings] theme]) {
        case UMThemeClassic:
            [[self uMaxItLabel] setAlpha:1.0f];
            [[self funLabel] setAlpha:0.0f];
            [[self ecoLabel] setAlpha:0.0f];
            break;
            
        case UMThemeModern:
            [[self uMaxItLabel] setAlpha:0.0f];
            [[self funLabel] setAlpha:1.0f];
            [[self ecoLabel] setAlpha:0.0f];
            break;

        case UMThemeEco:
            [[self uMaxItLabel] setAlpha:0.0f];
            [[self funLabel] setAlpha:0.0f];
            [[self ecoLabel] setAlpha:1.0f];
            break;

        default:
            break;
    }    
}

@end
