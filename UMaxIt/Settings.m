//
//  Copyright (c) 2013 York Block. All rights reserved.
//

#import "Settings.h"


static NSString * const UMIsSoundOnPrefKey    = @"UMIsSoundOnPrefKey";
static NSString * const UMThemePrefKey        = @"UMThemePrefKey";
static NSString * const UMIsNotFirstLaunchKey = @"UMIsNotFirstLaunchKey";


@implementation Settings

- (void)save
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setBool:([self isSoundOn] ? NO : YES) forKey:UMIsSoundOnPrefKey];
    [defaults setInteger:[self theme] forKey:UMThemePrefKey];
    [defaults synchronize];
}

- (void)load
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [self setIsSoundOn:([defaults boolForKey:UMIsSoundOnPrefKey] ? NO : YES)];
    [self setTheme:(UMTheme)[defaults integerForKey:UMThemePrefKey]];
}

+ (BOOL)isFirstLaunch
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    BOOL isFirstLaunch = [defaults boolForKey:UMIsNotFirstLaunchKey] ? NO : YES;
    [defaults setBool:YES forKey:UMIsNotFirstLaunchKey];
    [defaults synchronize];

    return isFirstLaunch;
}

@end
