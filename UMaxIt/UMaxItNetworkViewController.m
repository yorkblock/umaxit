//
//  Copyright (c) 2020 York Block. All rights reserved.
//

#import <GameKit/GameKit.h>

#import "UMaxItNetworkViewController.h"
#import "MatchmakerTableViewController.h"

static const int ddLogLevel = LOG_LEVEL_VERBOSE;


@interface UMaxItNetworkViewController () <MatchmakerTableViewControllerDelegate, GKLocalPlayerListener>

@property (nonatomic, weak)   NSTimer *gamePollingTimer;

@end


@implementation UMaxItNetworkViewController

//
// MARK: Init methods
//
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self addExitButton];
    [self setGameModel:[[UMaxItModel alloc] initWithMode:UMNetwork]];
    
    [[GKLocalPlayer localPlayer] registerListener:self];
    [[self gameModel] updateLocalPlayerId:[[GKLocalPlayer localPlayer] playerID]
                                    alias:[[GKLocalPlayer localPlayer] alias]];
    
    if ([self match])
        [self syncLocalWithGameCenter];
    else
        [self performSegueWithIdentifier:@"ShowMatchMaker" sender:self];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(syncGameCenterWithLocal:) name:UMGameDidUpdateNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self stopPollingGame];

    [[NSNotificationCenter defaultCenter] removeObserver:self name:UMGameDidUpdateNotification object:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqual:@"ShowMatchMaker"] == NO)
        return;
            
    MatchmakerTableViewController *vc = [[[segue destinationViewController] childViewControllers] objectAtIndex:0];
    [vc setDelegate:self];
}

//
// Mark: Bar buttons
//
- (void)addExitButton
{
    UIBarButtonItem *buttonItem = [[UIBarButtonItem alloc] initWithTitle:@"\uF08B" style:UIBarButtonItemStyleDone target:self action:@selector(quitGame)];
    [buttonItem setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"FontAwesome" size:24],
                                         NSForegroundColorAttributeName: [UIColor lightGrayColor]}
                              forState:UIControlStateNormal];
    [buttonItem setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"FontAwesome" size:24],
                                         NSForegroundColorAttributeName: [UIColor grayColor]}
                              forState:UIControlStateHighlighted];
    [[self navigationItem] setRightBarButtonItem:buttonItem];
}

- (void)quitGame
{
    UMGameState state = [[self gameModel] gameState];
    GKTurnBasedMatchStatus status = [[self match] status];
    if (state == UMGameIdle && status == GKTurnBasedMatchStatusMatching)
        [self deleteGame];
    else if (state == UMGameOver && status == GKTurnBasedMatchStatusEnded)
        [[self navigationController] popViewControllerAnimated:YES];
    else
        [[self gameModel] endGameLocalPlayerOutcome:UMOutcomeQuit opponentPlayerOutcome:UMOutcomeWon];
}


//
// MARK: Listener methods for GKLocalPlayerListener
//
- (void)turnBasedMatchmakerTableViewControllerWasCancelled:(MatchmakerTableViewController *)viewController
{
    [[self navigationController] popViewControllerAnimated:YES];
}

- (void)turnBasedMatchmakerTableViewController:(MatchmakerTableViewController *)viewController didFailWithError:(NSError *)error
{
    DDLogError(@"%@", [error localizedDescription]);
    [[self navigationController] popViewControllerAnimated:YES];
}

- (void)turnBasedMatchmakerTableViewController:(MatchmakerTableViewController *)viewController didLoadMatch:(GKTurnBasedMatch *)match
{
    [self setMatch:match];
    [self syncLocalWithGameCenter];
}

- (void)player:(GKPlayer *)player matchEnded:(GKTurnBasedMatch *)match
{
    DDLogVerbose(@"MatchEnded");
    
    if ([self match] && [[[self match] matchID] isEqualToString:[match matchID]] == NO)
        return;
    
    [self setMatch:match];
    [[self gameModel] setGame:[self loadGame]];
    [[self uMaxItView] setNeedsDisplay];
}

- (void)player:(GKPlayer *)player wantsToQuitMatch:(GKTurnBasedMatch *)match
{
    DDLogVerbose(@"WantsToQuitMatch");
    
    [self setMatch:match];
    [self quitGameTriggeredByCurrentPlayer];
}

- (void)player:(GKPlayer *)player receivedTurnEventForMatch:(GKTurnBasedMatch *)match didBecomeActive:(BOOL)didBecomeActive
{
    DDLogVerbose(@"G ReceivedTurnEventForMatch active(%d)", didBecomeActive);
    
    if ([self match] && [[[self match] matchID] isEqualToString:[match matchID]] == NO)
        return;
    
    [self setMatch:match];
    [self syncLocalWithGameCenter];
}

- (void)player:(GKPlayer *)player didRequestMatchWithOtherPlayers:(NSArray<GKPlayer *> *)playersToInvite
{
    DDLogVerbose(@"DidRequestMatchWithOtherPlayers");
}


//
// MARK: Sync methods
//
- (void)updatePlayer:(NSNotification *)notification
{
    [[self gameModel] updateLocalPlayerId:[[GKLocalPlayer localPlayer] playerID]
                                    alias:[[GKLocalPlayer localPlayer] alias]];
    [[self uMaxItView] setNeedsDisplay];
}

- (void)syncGameCenterWithLocal:(NSNotification *)notification
{
    if ([[self gameModel] isGameOver]) {
        [self endGame];
        [[self uMaxItView] setNeedsDisplay];
    } else
        [self endTurn];
}

- (void)syncLocalWithGameCenter
{
    if ([self otherPaticipantQuit]) {
        [self quitGameTriggeredByOtherParticipant];
    } else if (![self loadGame]) {
        [self endTurn];
    } else {
        [[self gameModel] setGame:[self loadGame]];
        [[self gameModel] updatePlayersInfoOnCompletion:^{
            if ([[self match] status] == GKTurnBasedMatchStatusOpen)
                [self endTurn];
        }];
    }
    
    [[self uMaxItView] setNeedsDisplay];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 13.0 && [[self gameModel] isGameOver] == NO)
        [self startPollingGame];
}

- (void)quitGameTriggeredByCurrentPlayer
{
    [[self gameModel] setGame:[self loadGame]];
    [self quitGame];
    [[self uMaxItView] setNeedsDisplay];
}

- (void)quitGameTriggeredByOtherParticipant
{
    [[self gameModel] setGame:[self loadGame]];
    [[self gameModel] endGameLocalPlayerOutcome:UMOutcomeWon opponentPlayerOutcome:UMOutcomeQuit];
    [[self uMaxItView] setNeedsDisplay];
}

- (NSMutableDictionary *)loadGame {
    if (![[self match] matchData])
        return nil;
    
    NSError *error = nil;
    NSSet *classes = [NSSet setWithObjects:[NSDictionary class], [NSMutableArray class], [NSDate class], nil];
    NSDictionary *game = [NSKeyedUnarchiver unarchivedObjectOfClasses:classes
                                                             fromData:[[self match] matchData]
                                                                error:&error];
    
    if (error)
        DDLogError(@"%@", [error description]);
    
    return [game mutableCopy];
}

- (void)saveGame
{
    NSDictionary *game = [[self gameModel] game];
    NSError *error = nil;
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:game requiringSecureCoding:NO error:&error];
    
    if (error) {
        DDLogError(@"%@", [error description]);
        return;
    }
    
    [[self match] saveCurrentTurnWithMatchData:data completionHandler:^(NSError * _Nullable error) {
        if (error)
            DDLogError(@"%@", [error description]);
    }];
}

- (void)endTurn
{
    NSDictionary *game = [[self gameModel] game];
    NSError *error;
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:game requiringSecureCoding:NO error:&error];
    
    if (error) {
        DDLogError(@"%@", [error description]);
        return;
    }
    
    DDLogVerbose(@"Save Game size (%ld)", [data length]);
    NSMutableArray *others = [[NSMutableArray alloc] init];
    for (GKTurnBasedParticipant *participant in [[self match] participants]) {
        if ([participant isEqual:[[self match] currentParticipant]])
            continue;
        
        [others addObject:participant];
    }
    
    [[self match] endTurnWithNextParticipants:others turnTimeout:GKTurnTimeoutNone matchData:data completionHandler:^(NSError * _Nullable error) {
        if (error)
            DDLogError(@"Error on end turn (%@)", [error description]);
    }];
}

- (void)endGame
{
    NSDictionary *game = [[self gameModel] game];
    GKTurnBasedMatchOutcome localOutcome = [self gameKitOutcomeFrom:[[self gameModel] localPlayerOutcome]];
    GKTurnBasedMatchOutcome opponentOutcome = [self gameKitOutcomeFrom:[[self gameModel] opponentPlayerOutcome]];
    NSError *error;
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:game requiringSecureCoding:NO error:&error];
    
    if (error)
        DDLogError(@"%@", [error description]);
    
    __weak UMaxItNetworkViewController *weakSelf = self;
    [[self match] saveCurrentTurnWithMatchData:data completionHandler:^(NSError * _Nullable error) {
        for (GKTurnBasedParticipant *participant in [[weakSelf match] participants]) {
            if ([[[participant player] playerID] isEqualToString:[[GKLocalPlayer localPlayer] playerID]])
                [participant setMatchOutcome:localOutcome];
            else
                [participant setMatchOutcome:opponentOutcome];
        }
        
        NSString *currentPartipantPlayerID = [[[[weakSelf match] currentParticipant] player] playerID];
        NSString *localPlayerID = [[GKLocalPlayer localPlayer] playerID];
        if ([currentPartipantPlayerID isEqualToString:localPlayerID]) {
            [[weakSelf match] endMatchInTurnWithMatchData:data completionHandler:^(NSError * _Nullable error) {
                if (error)
                    DDLogError(@"Error on end match (%@)", [error description]);
            }];
        } else {
            [[weakSelf match] participantQuitOutOfTurnWithOutcome:GKTurnBasedMatchOutcomeQuit withCompletionHandler:^(NSError * _Nullable error) {
                if (error)
                    DDLogError(@"Error on end match (%@)", [error description]);
            }];
        }
    }];
}

- (void)deleteGame
{
    __weak UMaxItNetworkViewController *weakSelf = self;
    [[self match] removeWithCompletionHandler:^(NSError * _Nullable error) {
                if (error) {
            DDLogError(@"Error: %@", [error localizedDescription]);
            //[[weakSelf activityIndicatorView] stopAnimating];
            return;
        }
        
        //[[weakSelf activityIndicatorView] stopAnimating];
        [[weakSelf navigationController] popViewControllerAnimated:YES];
    }];
}

//
// MARK: Add polling needed for iOS 12
//
- (void)startPollingGame
{
    if (_gamePollingTimer) {
        return;
    }
    _gamePollingTimer = [NSTimer scheduledTimerWithTimeInterval:5.0
                                                        repeats:YES
                                                          block:^(NSTimer * _Nonnull timer) {
        NSString *matchID = [[self match] matchID];
        [GKTurnBasedMatch loadMatchWithID:matchID withCompletionHandler:^(GKTurnBasedMatch * _Nullable match, NSError * _Nullable error) {
            if (error) {
                DDLogError(@"Load match data (%@)", [error description]);
                return;
            }
            
            NSString *localPlayerID = [[GKLocalPlayer localPlayer] playerID];
            NSString *currentPlayerID = [[[match currentParticipant] player] playerID];
            if ([localPlayerID isEqualToString:currentPlayerID]) {
                [self player:[GKLocalPlayer localPlayer] receivedTurnEventForMatch:match didBecomeActive:NO];
            } else if ([match status] == GKTurnBasedMatchStatusEnded) {
                [self player:[GKLocalPlayer localPlayer] matchEnded:match];
            }
        }];
    }];
}

- (void)stopPollingGame
{
    DDLogVerbose(@"Stop Polling");
    [[self gamePollingTimer] invalidate];
}


//
// MARK: Utility methods
//
- (BOOL)otherPaticipantQuit
{
    for (GKTurnBasedParticipant *participant in [[self match] participants]) {
        if ([participant isEqual:[[self match] currentParticipant]])
            continue;
        
        return [participant matchOutcome] == GKTurnBasedMatchOutcomeQuit && [[self match] status] != GKTurnBasedMatchStatusEnded;
    }
    
    return NO;
}

- (GKTurnBasedMatchOutcome)gameKitOutcomeFrom:(UMOutcome)uMaxItOutcome
{
    switch (uMaxItOutcome) {
        case UMOutcomeWon:  return GKTurnBasedMatchOutcomeWon;
        case UMOutcomeLost: return GKTurnBasedMatchOutcomeLost;
        case UMOutcomeQuit: return GKTurnBasedMatchOutcomeQuit;
        case UMOutcomeTied: return GKTurnBasedMatchOutcomeTied;
        case UMOutcomeNone: return GKTurnBasedMatchOutcomeNone;
    }
    
    return GKTurnBasedMatchOutcomeNone;
}


@end
