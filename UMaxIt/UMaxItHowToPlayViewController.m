//
//  Copyright (c) 2020 York Block. All rights reserved.
//

#import "UMaxItHowToPlayViewController.h"


typedef enum UMHowTo {
    UMHowToGreeting = 0,
    UMHowToPlayers,
    UMHowToBoard,
    UMHowToSelectRow,
    UMHowToSelectColumn,
    UMHowToGameOver,
    UMHowToEnd
} UMHowTo;


static NSString * const UMHowToGreetingMessage = @"Hi! This a quick tutorial of how to play this simple 80s pc game. "
                                                  "Tap here to continue";
static NSString * const UMHowToPlayersMessage  = @"You can play against your device, another opponent in the "
                                                  "same device or over the network with Game Center.";
static NSString * const UMHowToBoardMessage    = @"At the start, the board is filled with 64 random numbers. "
                                                  "One of the squares is selected as the initial position. "
                                                  "The selected square becomes a pivot, you can choose to play in one "
                                                  "of it axis.";
static NSString * const UMHowToRowMessage      = @"Once you select a number from one of the axis, you are only allowed to "
                                                  "play in the same axis for the rest of the game. The number is added "
                                                  "to your score and the turn is passed to the opponent.";
static NSString * const UMHowToColumnMessage   = @"Since you chose to play in the X axis, the opponent must choose a "
                                                  "number from the Y axis using the selected square as the pivot. "
                                                  "The number is added and the turn returns to you.";
static NSString * const UMHowToGameOverMessage = @"The game goes on until no more numbers can be selected. Enjoy!";


@interface UMaxItHowToPlayViewController ()

@property (nonatomic, weak) IBOutlet UIView             *scoresContainerView;
@property (nonatomic, weak) IBOutlet UIView             *boardContainerView;
@property (nonatomic, weak) IBOutlet UIView             *turnContainerView;
@property (nonatomic, weak) IBOutlet UIView             *firstPlayerView;
@property (nonatomic, weak) IBOutlet UIView             *secondPlayerView;
@property (nonatomic, weak) IBOutlet UIView             *howToContainerView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *howToContainerYLocation;
@property (nonatomic, weak) IBOutlet UILabel            *messageLabel;

@property (nonatomic) UMHowTo step;

@end


@implementation UMaxItHowToPlayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[[self howToContainerView] layer] setCornerRadius:4.0f];
    [[[self howToContainerView] layer] setBorderWidth:1.0f];
    [[[self howToContainerView] layer] setBorderColor:[[UIColor lightGrayColor] CGColor]];
    [[[self howToContainerView] layer] setMasksToBounds:YES];
    
    [[self howToContainerView] setHidden:YES];
    [[self howToContainerYLocation] setConstant:-[[self howToContainerView] bounds].size.height];
    
    UMaxItModel *model = [[UMaxItModel alloc] initWithMode:UMTwoPlayers];
    [self setGameModel:model];
    
    [self setStep:UMHowToGreeting];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self showNextStep:nil];
}

- (IBAction)showNextStep:(id)sender
{
    switch ([self step]) {
        case UMHowToGreeting: [self showGreeting]; break;
        case UMHowToPlayers: [self showPlayers]; break;
        case UMHowToBoard: [self showBoard]; break;
        case UMHowToSelectRow: [self showFirstPlayerMove]; break;
        case UMHowToSelectColumn: [self showSecondPlayerMove]; break;
        case UMHowToGameOver: [self showGameOver]; break;
        case UMHowToEnd: [self dismiss]; break;
    }
    [self setStep:[self step] + 1];
}

- (void)showGreeting
{
    [[self howToContainerView] setHidden:NO];
    [self showMessage:UMHowToGreetingMessage];
}

- (void)showPlayers
{
    [self showMessage:UMHowToPlayersMessage];
    
    [UIView animateWithDuration:0.3
                          delay:0.6
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{ [self scaleUpPlayerView:[self firstPlayerView]]; }
                     completion:^(BOOL finished) { [self scaleDownPlayerView:[self firstPlayerView]]; }];
    
    [UIView animateWithDuration:0.3
                          delay:1.5
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{ [self scaleUpPlayerView:[self secondPlayerView]]; }
                     completion:^(BOOL finished) { [self scaleDownPlayerView:[self secondPlayerView]]; }];
}

- (void)scaleUpPlayerView:(UIView *)playerView
{
    [playerView setTransform:CGAffineTransformMakeScale(1.25, 1.25)];
}

- (void)scaleDownPlayerView:(UIView *)playerView
{
    [UIView animateWithDuration:0.3
                     animations:^{ [playerView setTransform:CGAffineTransformMakeScale(1.0, 1.0)]; }
                     completion:nil];
}

- (void)showBoard
{
    [self showMessage:UMHowToBoardMessage];
}

- (void)showFirstPlayerMove
{
    [self showMessage:UMHowToRowMessage];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self playToColPostion];
    });
}

- (void)showSecondPlayerMove
{
    [self showMessage:UMHowToColumnMessage];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self playToRowPosition];
    });
}

- (void)showGameOver
{
    [self showMessage:UMHowToGameOverMessage];
    [self playToGameOver];
}

- (void)dismiss
{
    [[self howToContainerYLocation] setConstant:[[self view] bounds].size.height];
    [UIView animateWithDuration:0.3f
                          delay:0.3f
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{ [[self view] layoutIfNeeded]; }
                     completion:^(BOOL finished) { if (finished) [[self presentingViewController] dismissViewControllerAnimated:YES completion:nil]; }];
}

- (void)showMessage:(NSString *)message
{
    CGFloat turnY = [[self turnContainerView] frame].origin.y;
    CGFloat turnH = [[self turnContainerView] bounds].size.height;
    [[self howToContainerYLocation] setConstant:turnY + turnH + 20.0f];
    
    [UIView animateWithDuration:0.3f
                          delay:0.3f
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{ [[self view] layoutIfNeeded]; }
                     completion:^(BOOL finished) { if (finished) [[self messageLabel] setText:message]; }];
}

- (void)playToColPostion
{
    UMSquare location = [[self gameModel] cursorLocation];
    
    for (int i = 0; i < 8; i++) {
        location.col = location.col + 1 > 7 ? 0 : location.col + 1;
        id object = [[[self gameModel] board] objectAtIndex:UMSquareToIndex(location)];
        
        if ([object isKindOfClass:[NSNumber class]])
            break;
    }
    
    [[self gameModel] playToPosition:location];
    [[self uMaxItView] setNeedsDisplay];
}

- (void)playToRowPosition
{
    UMSquare location = [[self gameModel] cursorLocation];
    
    for (int i = 0; i < 8; i++) {
        location.row = location.row + 1 > 7 ? 0 : location.row + 1;
        id object = [[[self gameModel] board] objectAtIndex:UMSquareToIndex(location)];
        
        if ([object isKindOfClass:[NSNumber class]])
            break;
    }
    
    [[self gameModel] playToPosition:location];
    [[self uMaxItView] setNeedsDisplay];
}

- (void)playToGameOver
{
    static int step = 0;
    
    if ([[self gameModel] isGameOver]) {
        step = 0;
        return;
    }
    
    step % 2 == 0 ? [self playToColPostion] : [self playToRowPosition];
    step++;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self playToGameOver];
    });
}

- (void)squareSelectionGesture:(UITapGestureRecognizer *)gesture
{
}

@end
