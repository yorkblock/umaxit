//
//  Copyright (c) 2020 York Block. All rights reserved.
//


#import "MatchmakerTableViewController.h"
#import "MatchmakerGameTableViewCell.h"
#import "DDLog.h"


static const int ddLogLevel = LOG_LEVEL_VERBOSE;


@interface MatchmakerTableViewController () <MatchmakerGameTableViewCellDelegate>

@property (nonatomic, strong) UIActivityIndicatorView *activityIndicatorView;
@property (nonatomic, strong) UILabel                 *titleLabel;
@property (nonatomic, strong) UIBarButtonItem         *closeButton;
@property (nonatomic, strong) UIBarButtonItem         *inviteButton;
@property (nonatomic, strong) UIBarButtonItem         *automatchButton;
@property (nonatomic, strong) NSArray                 *matches;

@end


@implementation MatchmakerTableViewController

@synthesize activityIndicatorView = _activityIndicatorView;
- (UIActivityIndicatorView *)activityIndicatorView
{
    if (!_activityIndicatorView) {
        _activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [_activityIndicatorView setColor:[UIColor grayColor]];
        [_activityIndicatorView setHidesWhenStopped:YES];
        [_activityIndicatorView stopAnimating];
        [[self view] addSubview:_activityIndicatorView];
        [_activityIndicatorView setCenter:[[self view] center]];
    }
    
    return _activityIndicatorView;
}

@synthesize titleLabel = _titleLabel;
- (UILabel *)titleLabel
{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 50.0, 30.0)];
        
        NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:@"Arial Rounded MT Bold" size:24],
                                     NSForegroundColorAttributeName: [UIColor lightGrayColor]};
        [_titleLabel setAttributedText:[[NSAttributedString alloc] initWithString:@"UMaxIt"
                                                                       attributes:attributes]];
        [_titleLabel setTextColor:[UIColor lightGrayColor]];
    }
    
    return _titleLabel;
}

@synthesize closeButton = _closeButton;
- (UIBarButtonItem *)closeButton
{
    if (!_closeButton) {
        _closeButton = [[UIBarButtonItem alloc] initWithTitle:@"\uF00D"
                                                        style:UIBarButtonItemStyleDone
                                                       target:self
                                                       action:@selector(close)];
        
        [_closeButton setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"FontAwesome" size:24],
                                               NSForegroundColorAttributeName: [UIColor lightGrayColor]}
                                    forState:UIControlStateNormal];
        [_closeButton setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"FontAwesome" size:24],
                                               NSForegroundColorAttributeName: [UIColor grayColor]}
                                    forState:UIControlStateHighlighted];
    }
    
    return _closeButton;
}

@synthesize inviteButton = _inviteButton;
- (UIBarButtonItem *)inviteButton
{
    if (!_inviteButton) {
        _inviteButton = [[UIBarButtonItem alloc] initWithTitle:@"Invite"
                                                         style:UIBarButtonItemStyleDone
                                                        target:self
                                                        action:@selector(inviteFriend)];
        
        [_inviteButton setTitleTextAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Arial Rounded MT Bold" size:16],
                                                NSForegroundColorAttributeName: [UIColor lightGrayColor]}
                                     forState:UIControlStateNormal];
        [_inviteButton setTitleTextAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Arial Rounded MT Bold" size:16],
                                                NSForegroundColorAttributeName: [UIColor grayColor]}
                                     forState:UIControlStateHighlighted];
    }
    
    return _inviteButton;
}

@synthesize automatchButton = _automatchButton;
- (UIBarButtonItem *)automatchButton
{
    if (!_automatchButton) {
        _automatchButton = [[UIBarButtonItem alloc] initWithTitle:@"Automatch"
                                                         style:UIBarButtonItemStyleDone
                                                        target:self
                                                        action:@selector(automatch)];
        
        [_automatchButton setTitleTextAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Arial Rounded MT Bold" size:16],
                                                NSForegroundColorAttributeName: [UIColor lightGrayColor]}
                                     forState:UIControlStateNormal];
        [_automatchButton setTitleTextAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Arial Rounded MT Bold" size:16],
                                                NSForegroundColorAttributeName: [UIColor grayColor]}
                                     forState:UIControlStateHighlighted];
    }
    
    return _automatchButton;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[self navigationItem] setTitleView:[self titleLabel]];
    [[self navigationItem] setLeftBarButtonItem:[self closeButton]];
    
    [self setToolbarItems:@[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [self automatchButton],
                            [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil]]];
    
    [self loadGames];
}

- (void)close
{
    [[self presentingViewController] dismissViewControllerAnimated:YES completion:nil];
    [[self delegate] turnBasedMatchmakerTableViewControllerWasCancelled:self];
}

- (void)inviteFriend
{
}

- (void)automatch
{
    [[self activityIndicatorView] startAnimating];
    
    GKMatchRequest *request = [[GKMatchRequest alloc] init];
    request.maxPlayers = 2;
    request.minPlayers = 2;
    
    __weak MatchmakerTableViewController *weakSelf = self;
    [GKTurnBasedMatch findMatchForRequest:request withCompletionHandler:^(GKTurnBasedMatch * _Nullable match, NSError * _Nullable error) {
        if (error) {
            DDLogError(@"%@", [error localizedDescription]);
            [[weakSelf presentingViewController] dismissViewControllerAnimated:YES
                                                                    completion:^{
                [[weakSelf delegate] turnBasedMatchmakerTableViewController:weakSelf didFailWithError:error];
            }];
            
            return;
        }
        
        [[weakSelf delegate] turnBasedMatchmakerTableViewController:weakSelf didLoadMatch:match];
        [[weakSelf presentingViewController] dismissViewControllerAnimated:YES completion:nil];
    }];
}

- (void)loadGames
{
    __weak MatchmakerTableViewController *weakSelf = self;
    [GKTurnBasedMatch loadMatchesWithCompletionHandler:^(NSArray<GKTurnBasedMatch *> * _Nullable matches, NSError * _Nullable error) {
        [weakSelf setMatches:[weakSelf sortMatches:matches]];
        [[weakSelf tableView] reloadData];
    }];
}

- (NSArray *)sortMatches:(NSArray *)matches
{
    NSArray *matchingMatches = [matches objectsAtIndexes:[matches indexesOfObjectsPassingTest:^BOOL(GKTurnBasedMatch * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        return [obj status] == GKTurnBasedMatchStatusMatching;
    }]];
    
    NSArray *sortedMatchingMatches = [matchingMatches sortedArrayUsingComparator:^NSComparisonResult(GKTurnBasedMatch *obj1, GKTurnBasedMatch *obj2) {
        return [[obj2 creationDate] compare:[obj1 creationDate]];
    }];
    
    NSArray *playingMatches = [matches objectsAtIndexes:[matches indexesOfObjectsPassingTest:^BOOL(GKTurnBasedMatch * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        return [obj status] == GKTurnBasedMatchStatusOpen;
    }]];
    
    NSArray *sortedPlayingMatches = [playingMatches sortedArrayUsingComparator:^NSComparisonResult(GKTurnBasedMatch *obj1, GKTurnBasedMatch *obj2) {
        return [[[obj2 currentParticipant] lastTurnDate] compare:[[obj1 currentParticipant] lastTurnDate]];
    }];
    
    NSArray *endedMatches = [matches objectsAtIndexes:[matches indexesOfObjectsPassingTest:^BOOL(GKTurnBasedMatch * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        return [obj status] == GKTurnBasedMatchStatusEnded;
    }]];
    
    NSArray *sortedEndedMatches = [endedMatches sortedArrayUsingComparator:^NSComparisonResult(GKTurnBasedMatch *obj1, GKTurnBasedMatch *obj2) {
        return [[obj2 creationDate] compare:[obj1 creationDate]];
    }];
    
    return [sortedMatchingMatches arrayByAddingObjectsFromArray:[sortedPlayingMatches arrayByAddingObjectsFromArray:sortedEndedMatches]];
}


//
// MARK: - Table view data source
//
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self matches] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MatchmakerGameTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GameCell" forIndexPath:indexPath];
    
    [cell setMatch:[[self matches] objectAtIndex:[indexPath row]]];
    [cell setDelegate:self];
    
    return cell;
}


//
// MARK: - Table view delegate
//
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[self activityIndicatorView] isAnimating])
        return;
    
    [[self activityIndicatorView] startAnimating];
    GKTurnBasedMatch *match = [[self matches] objectAtIndex:[indexPath row]];
    __weak MatchmakerTableViewController *weakSelf = self;
    [match loadMatchDataWithCompletionHandler:^(NSData * _Nullable matchData, NSError * _Nullable error) {
        if (error) {
            DDLogError(@"%@", [error localizedDescription]);
            [[weakSelf presentingViewController] dismissViewControllerAnimated:YES
                                                                    completion:^{
                [[weakSelf delegate] turnBasedMatchmakerTableViewController:weakSelf didFailWithError:error];
            }];
            
            return;
        }
                
        [[weakSelf delegate] turnBasedMatchmakerTableViewController:weakSelf didLoadMatch:match];
        [[weakSelf presentingViewController] dismissViewControllerAnimated:YES completion:nil];
    }];
}


//
// MARK: - MatchmakerGameTableViewCellDelegate
//
- (void)matchmakerGameTableViewCell:(MatchmakerGameTableViewCell *)cell requestsToDeleteMatch:(GKTurnBasedMatch *)match
{
    if ([[self activityIndicatorView] isAnimating])
        return;
    
    [[self activityIndicatorView] startAnimating];
    __weak MatchmakerTableViewController *weakSelf = self;
    [match removeWithCompletionHandler:^(NSError * _Nullable error) {
        if (error) {
            DDLogError(@"Error: %@", [error localizedDescription]);
            [[weakSelf activityIndicatorView] stopAnimating];
            return;
        }
        
        [[weakSelf activityIndicatorView] stopAnimating];
        [weakSelf loadGames];
    }];
}

@end
