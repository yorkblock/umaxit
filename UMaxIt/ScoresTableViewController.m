//
//  Copyright (c) 2012 York Block. All rights reserved.
//

#import "ScoresTableViewController.h"
#import "GameCenterScoresTableViewController.h"
#import "UMaxIt.h"
#import "LocalScores.h"


typedef enum UMTableScores {
    UMTableTopScores,
    UMTableLatestScores
} UMTableScores;


@interface ScoresTableViewController () <UITableViewDataSource>

@property (nonatomic, weak) IBOutlet UILabel *rankLabel;
@property (nonatomic, weak) IBOutlet UILabel *gamerLabel;
@property (nonatomic, weak) IBOutlet UILabel *scoreLabel;

@property (nonatomic, strong) NSArray        *scores;

@end


@implementation ScoresTableViewController

//
// MARK: Actions and Segues methods
//
- (IBAction)showGlobalScores:(id)sender
{
    [self performSegueWithIdentifier:@"ShowGameCenterScores" sender:self];
}

//
// MARK: Initialization, (Un)Loads, and such methods
//
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 50.0, 30.0)];
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:@"Arial Rounded MT Bold" size:24],
                                 NSForegroundColorAttributeName: [UIColor lightGrayColor]};
    NSString *title = @"Local";
    [titleLabel setAttributedText:[[NSAttributedString alloc] initWithString:title
                                                                  attributes:attributes]];
    [titleLabel setTextColor:[UIColor lightGrayColor]];
    [[self navigationItem] setTitleView:titleLabel];
    
    [[self rankLabel] setText:[NSString stringWithFormat:@"%@", NSLocalizedString(@"RANK", nil)]];
    [[self gamerLabel] setText:[NSString stringWithFormat:@"%@", NSLocalizedString(@"GAMER", nil)]];
    [[self scoreLabel] setText:[NSString stringWithFormat:@"%@", NSLocalizedString(@"SCORE", nil)]];
    
    UIBarButtonItem *globalScoreBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"\uF0AC"
                                                                                 style:UIBarButtonItemStyleDone
                                                                                target:self
                                                                                action:@selector(showGlobalScores:)];
    [globalScoreBarButtonItem setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"FontAwesome" size:24],
                                                       NSForegroundColorAttributeName: [UIColor lightGrayColor]}
                                            forState:UIControlStateNormal];
    [globalScoreBarButtonItem setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"FontAwesome" size:24],
                                                       NSForegroundColorAttributeName: [UIColor grayColor]}
                                            forState:UIControlStateHighlighted];
    [[self navigationItem] setRightBarButtonItem:globalScoreBarButtonItem];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"\uF060"
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:nil
                                                                  action:NULL];
    [backButton setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"FontAwesome" size:24],
                                         NSForegroundColorAttributeName: [UIColor lightGrayColor]}
                              forState:UIControlStateNormal];
    [backButton setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"FontAwesome" size:24],
                                         NSForegroundColorAttributeName: [UIColor grayColor]}
                              forState:UIControlStateHighlighted];
    [[self navigationItem] setBackBarButtonItem:backButton];
    
    [self setScores:[LocalScores load]];
}


//
// MARK: UITableViewDataSource implementation
//
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell
forRowAtIndexPath:(NSIndexPath *)indexPath
{    
    cell.backgroundColor = ((indexPath.row % 2) == 0) ? UIColorFromRGB(0xfafafa) : UIColorFromRGB(0xffffff);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self scores] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Score Cell Table Reusable Identifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    UILabel *rankLabel = (UILabel *)[cell viewWithTag:1];
    UILabel *aliasLabel = (UILabel *)[cell viewWithTag:2];
    UILabel *scoreLabel = (UILabel *)[cell viewWithTag:3];
    
    [rankLabel setText:[NSString stringWithFormat:@"%02ld", (long)[indexPath row] + 1]];
    [scoreLabel setText:[NSString stringWithFormat:@"%ld", labs((long)[[[self scores] objectAtIndex:[indexPath row]] integerValue])]];
    if ([[[self scores] objectAtIndex:[indexPath row]] integerValue] >= 0) {
        [aliasLabel setText:@"ME"];
        [scoreLabel setTextColor:[UIColor blueColor]];
    } else {
        [aliasLabel setText:@"AI"];
        [scoreLabel setTextColor:[UIColor redColor]];
    }
    
    return cell;
}

@end
