//
//  Copyright (c) 2020 York Block. All rights reserved.
//

#import "UMaxItTwoPlayersViewController.h"


@interface UMaxItTwoPlayersViewController ()

@end


@implementation UMaxItTwoPlayersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self addRestartButton];
    [self reset];
}

- (void)reset
{
    [self setGameModel:[[UMaxItModel alloc] initWithMode:UMTwoPlayers]];
    [[self uMaxItView] setNeedsDisplay];
}

- (void)addRestartButton
{
    UIBarButtonItem *buttonItem = [[UIBarButtonItem alloc] initWithTitle:@"\uF021" style:UIBarButtonItemStyleDone target:self action:@selector(reset)];
    [buttonItem setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"FontAwesome" size:24],
                                         NSForegroundColorAttributeName: [UIColor lightGrayColor]}
                              forState:UIControlStateNormal];
    [buttonItem setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"FontAwesome" size:24],
                                         NSForegroundColorAttributeName: [UIColor grayColor]}
                              forState:UIControlStateHighlighted];
    [[self navigationItem] setRightBarButtonItem:buttonItem];
}

@end
