//
//  Copyright (c) 2013 York Block. All rights reserved.
//

#import <GameKit/GameKit.h>

#import "UMaxIt.h"
#import "GameCenterScoresTableViewController.h"


@interface GameCenterScoresTableViewController ()

@property (nonatomic, weak)   IBOutlet UILabel *rankLabel;
@property (nonatomic, weak)   IBOutlet UILabel *gamerLabel;
@property (nonatomic, weak)   IBOutlet UILabel *scoreLabel;

@property (nonatomic, strong)          NSArray *scores;

@end


@implementation GameCenterScoresTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[self rankLabel] setText:[NSString stringWithFormat:@"%@", NSLocalizedString(@"RANK", nil)]];
    [[self gamerLabel] setText:[NSString stringWithFormat:@"%@", NSLocalizedString(@"GAMER", nil)]];
    [[self scoreLabel] setText:[NSString stringWithFormat:@"%@", NSLocalizedString(@"SCORE", nil)]];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 50.0, 30.0)];
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:@"Arial Rounded MT Bold" size:24],
                                 NSForegroundColorAttributeName: [UIColor lightGrayColor]};
    NSString *title = @"Global";
    [titleLabel setAttributedText:[[NSAttributedString alloc] initWithString:title
                                                                  attributes:attributes]];
    [titleLabel setTextColor:[UIColor lightGrayColor]];
    [[self navigationItem] setTitleView:titleLabel];
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl setTintColor:[UIColor lightGrayColor]];
    [refreshControl addTarget:self action:@selector(loadScores:) forControlEvents:UIControlEventValueChanged];
    [self setRefreshControl:refreshControl];
    
    [[self tableView] setContentOffset:CGPointMake(0.0f, -1.0f * [[self refreshControl] frame].size.height)
                              animated:YES];
    [[self refreshControl] sendActionsForControlEvents:UIControlEventValueChanged];
}

- (void)loadScores:(id)sender
{
    [[self refreshControl] beginRefreshing];
    
    __weak GameCenterScoresTableViewController *weakSelf = self;
    [self loadScoresWithCompletionHandler:^(NSArray *scores, NSError *error) {
        [[weakSelf refreshControl] endRefreshing];

        if (error) {
            return;
        }
        
        [weakSelf setScores:scores];
        [[weakSelf tableView] reloadData];
    }];
}


//
// MARK: - Table view data source
//
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self scores] count];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell
forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = ((indexPath.row % 2) == 0) ? UIColorFromRGB(0xfbfbfb) : UIColorFromRGB(0xffffff);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Score Cell Table Reusable Identifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    UILabel *rankLabel = (UILabel *)[cell viewWithTag:1];
    UILabel *aliasLabel = (UILabel *)[cell viewWithTag:2];
    UILabel *scoreLabel = (UILabel *)[cell viewWithTag:3];
    
    GKScore *score = [[[self scores] objectAtIndex:[indexPath row]] objectForKey:@"scoreKey"];
    [rankLabel setText:[NSString stringWithFormat:@"%03ld", (long)[score rank]]];
    [scoreLabel setText:[NSString stringWithFormat:@"%03lld", [score value]]];
    
    GKPlayer *player = [[[self scores] objectAtIndex:[indexPath row]] objectForKey:@"playerKey"];
    [aliasLabel setText:[player alias]];
    
    return cell;
}


//
// MARK: Scores
//
- (void)loadScoresWithCompletionHandler:(void (^)(NSArray *scores, NSError *error))onCompletion
{
    __weak GameCenterScoresTableViewController *weakSelf = self;
    [GKLeaderboard loadLeaderboardsWithCompletionHandler:^(NSArray *leaderboards, NSError *error) {
        if (error) {
            onCompletion(nil, error);
            return;
        }
        
        [weakSelf retrieveScoresForLeaderboard:GameCenterLeaderboardHardCategory
                                   playerScope:GKLeaderboardPlayerScopeGlobal
                             matchingPlayerIds:nil
                                     timeScope:GKLeaderboardTimeScopeAllTime
                                         range:NSMakeRange(1,10)
                             completionHandler:onCompletion];
    }];
}

- (void)retrieveScoresForLeaderboard:(NSString *)category
                         playerScope:(GKLeaderboardPlayerScope)playerScope
                   matchingPlayerIds:(NSArray *)ids
                           timeScope:(GKLeaderboardTimeScope)timeScope
                               range:(NSRange)range
                   completionHandler:(void (^)(NSArray *scores, NSError *error))onCompletion
{
    GKLeaderboard *leaderboardRequest = ids ? [[GKLeaderboard alloc] initWithPlayers:ids] : [[GKLeaderboard alloc] init];
    [leaderboardRequest setIdentifier:category];
    [leaderboardRequest setPlayerScope:playerScope];
    [leaderboardRequest setTimeScope:timeScope];
    [leaderboardRequest setRange:range];
    
    __weak GameCenterScoresTableViewController *weakSelf = self;
    [leaderboardRequest loadScoresWithCompletionHandler:^(NSArray *scores, NSError *error) {
        if (error) {
            onCompletion(nil, error);
            return;
        }
        
        NSMutableArray *playersID = [[NSMutableArray alloc] init];
        for (GKScore *score in scores) {
            [playersID addObject:[[score player] playerID]];
        }
        
        [GKPlayer loadPlayersForIdentifiers:playersID withCompletionHandler:^(NSArray *players, NSError *error) {
            if (error) {
                onCompletion(nil, error);
                return;
            }
            
            onCompletion([weakSelf mergeScores:scores
                                       players:players
                           andLocalPlayerScore:[leaderboardRequest localPlayerScore]], nil);
        }];
    }];
}

- (NSArray *)mergeScores:(NSArray *)scores players:(NSArray *)players andLocalPlayerScore:(GKScore *)localScore
{
    NSMutableArray *scoresAndPlayers = [[NSMutableArray alloc] init];
    BOOL isLocalPlayerFound = NO;
    
    for (GKScore *score in scores) {
        NSUInteger idx = [players indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
            if ([[obj playerID] isEqualToString:[[score player] playerID]]) {
                *stop = YES;
                return YES;
            }
            return NO;
        }];
        
        if (idx != NSNotFound) {
            [scoresAndPlayers addObject:@{@"scoreKey": score, @"playerKey": players[idx]}];
            if ([[players objectAtIndex:idx] isKindOfClass:[GKLocalPlayer class]])
                isLocalPlayerFound = YES;
        }
    }
    
    if (!isLocalPlayerFound) {
        [scoresAndPlayers addObject:@{@"scoreKey": localScore, @"playerKey": [GKLocalPlayer localPlayer]}];
    }
    
    return scoresAndPlayers;
}

@end
