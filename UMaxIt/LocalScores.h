//
//  Copyright (c) 2020 York Block. All rights reserved.
//


#import <Foundation/Foundation.h>


NS_ASSUME_NONNULL_BEGIN

@interface LocalScores : NSObject

+ (void)store:(NSNumber *)score;
+ (NSArray *)load;

@end

NS_ASSUME_NONNULL_END
