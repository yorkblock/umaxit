//
//  Copyright (c) 2020 York Block. All rights reserved.
//

#import "LocalScores.h"
#import "UMaxIt.h"

@implementation LocalScores


+ (void)store:(NSNumber *)score
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    /*
     * Obtain list of top scores.
     */
    NSMutableArray *topScores = [[defaults arrayForKey:UMTopScoresPrefKey] mutableCopy];
    if (!topScores)
        topScores = [[NSMutableArray alloc] init];
    /*
     * Add score, sort them.
     */
    [topScores addObject:score];
    [topScores sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        if ([obj1 integerValue] > [obj2 integerValue]) {
            return NSOrderedAscending;
        } else if ([obj1 integerValue] < [obj2 integerValue]) {
            return NSOrderedDescending;
        } else {
            return NSOrderedSame;
        }
    }];
        
    /*
     * Store top scores.
     */
    NSInteger size = [topScores count];
    if (size > 20)
        size = 20;
    [defaults setObject:[topScores subarrayWithRange:NSMakeRange(0, size)] forKey:UMTopScoresPrefKey];
}

+ (NSArray *)load
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    return [defaults arrayForKey:UMTopScoresPrefKey];
}

@end
