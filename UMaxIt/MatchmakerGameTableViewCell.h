//
//  Copyright (c) 2020 York Block. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GameKit/GameKit.h>


@class MatchmakerGameTableViewCell;

@protocol MatchmakerGameTableViewCellDelegate <NSObject>

- (void)matchmakerGameTableViewCell:(MatchmakerGameTableViewCell *)cell requestsToDeleteMatch:(GKTurnBasedMatch *)match;

@end



@interface MatchmakerGameTableViewCell : UITableViewCell

@property (nonatomic, strong) GKTurnBasedMatch *match;

@property (nonatomic, weak) id<MatchmakerGameTableViewCellDelegate> delegate;

@end

