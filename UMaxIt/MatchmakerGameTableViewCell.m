//
//  Copyright (c) 2020 York Block. All rights reserved.
//

#import "MatchmakerGameTableViewCell.h"


@interface MatchmakerGameTableViewCell ()

@property (nonatomic, weak) IBOutlet UIImageView *avatarImageView;
@property (nonatomic, weak) IBOutlet UILabel     *dateLabel;
@property (nonatomic, weak) IBOutlet UILabel     *aliasLabel;
@property (nonatomic, weak) IBOutlet UILabel     *statusLabel;
@property (nonatomic, weak) IBOutlet UIButton    *actionButton;

@end


@implementation MatchmakerGameTableViewCell

@synthesize match = _match;
- (void)setMatch:(GKTurnBasedMatch *)match
{
    _match = match;
    [self updateCell];
}

- (void)updateCell
{
    [[self dateLabel] setText:[self date]];
    [[self aliasLabel] setText:[self alias]];
    [[self statusLabel] setText:[self status]];
    [[self actionButton] setTitle:[self action] forState:UIControlStateNormal];
    [self updateAvatar];
}

- (NSString *)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterMediumStyle];
    
    NSDate *date = [[[self match] currentParticipant] lastTurnDate];
    if (date)
        return [@"Last move " stringByAppendingString:[formatter stringFromDate:date]];
    
    return [@"Created " stringByAppendingString:[formatter stringFromDate:[[self match] creationDate]]];
}

- (NSString *)alias
{
    for (GKTurnBasedParticipant *participant in [[self match] participants]) {
        if ([[participant player] isEqual:[GKLocalPlayer localPlayer]] == NO) {
            return [[participant player] alias] ? [[participant player] alias] : @"Waiting...";
        }
    }
    
    return @"Waiting for opponent";
}

- (NSString *)status
{
    switch ([[self match] status]) {
        case GKTurnBasedMatchStatusOpen:
            return [self turn];
            
        case GKTurnBasedMatchStatusMatching:
            return @"Matching";
        
        case GKTurnBasedMatchStatusEnded:
            return [self outcome];
            
        case GKTurnBasedMatchStatusUnknown:
            return @"Unknown";
    }
    
    return @"";
}

- (NSString *)turn
{
    if ([[[self match] currentParticipant] isEqual:[GKLocalPlayer localPlayer]])
        return @"Turn: You";
    
    NSString *alias = [[[[self match] currentParticipant] player] alias];
    return alias ? [@"Turn: " stringByAppendingString:alias] : @"Matching";
}

- (NSString *)outcome
{
    for (GKTurnBasedParticipant *participant in [[self match] participants]) {
        if ([[participant player] isEqual:[GKLocalPlayer localPlayer]]) {
            switch ([participant matchOutcome]) {
                case GKTurnBasedMatchOutcomeWon:  return @"You Won";
                case GKTurnBasedMatchOutcomeLost: return @"You Lost";
                case GKTurnBasedMatchOutcomeQuit: return @"You Quit";
                case GKTurnBasedMatchOutcomeTied: return @"Both Tied";
                default: return @"None";
            }
        }
    }
    
    return @"Unknown";
}

- (void)updateAvatar
{
    for (GKTurnBasedParticipant *participant in [[self match] participants]) {
        if ([[participant player] isEqual:[GKLocalPlayer localPlayer]] == NO) {
            [[participant player] loadPhotoForSize:GKPhotoSizeSmall
                             withCompletionHandler:^(UIImage * _Nullable photo, NSError * _Nullable error) {
                if (error) {
                    NSLog(@"%@", [error description]);
                    return;
                }
                
                [[self avatarImageView] setImage:photo];
            }];
        }
    }
}

- (NSString *)action
{
    GKTurnBasedParticipant *localParticipant;
    if ([[[[[self match] participants] objectAtIndex:0] player] isEqual:[GKLocalPlayer localPlayer]])
        localParticipant = [[[self match] participants] objectAtIndex:0];
    else
        localParticipant = [[[self match] participants] objectAtIndex:1];
    
    if ([localParticipant matchOutcome] != GKTurnBasedMatchOutcomeNone)
        return @"\uF1F8";
    
    return nil;
}

- (IBAction)deleteMatch:(id)sender
{
    if ([[self match] status] == GKTurnBasedMatchStatusEnded) {
        [[self delegate] matchmakerGameTableViewCell:self requestsToDeleteMatch:[self match]];
    }
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
