//
//  Copyright (c) 2012 York Block. All rights reserved.
//

#import "UMaxItView.h"
#import "CircleView.h"


@interface UMaxItView ()

@property (nonatomic, weak) IBOutlet UILabel *firstPlayerLabel;
@property (nonatomic, weak) IBOutlet UILabel *firstPlayerScoreLabel;
@property (nonatomic, weak) IBOutlet UILabel *secondPlayerLabel;
@property (nonatomic, weak) IBOutlet UILabel *secondPlayerScoreLabel;
@property (nonatomic, weak) IBOutlet UIView  *turnContainerView;
@property (nonatomic, weak) IBOutlet UILabel *waitingForOpponentLabel;

@property (nonatomic, strong) NSArray       *squaresBoard;
@property (nonatomic)         CGRect         boardRect;
@property (nonatomic)         CGFloat        scale;
@property (nonatomic, strong) NSArray       *scoreBoard;
@property (nonatomic, strong) UIView        *verticalMaskView;
@property (nonatomic, strong) UIView        *horizontalMaskView;
@property (nonatomic, strong) UILabel       *finalScoreLabel;
@property (nonatomic, strong) UIFont        *viewFont;
@property (nonatomic, strong) NSDictionary  *viewStringAttributes;
@property (nonatomic, strong) NSArray       *colors;
@property (nonatomic, strong) CircleView    *turnView;

@end


@implementation UMaxItView

//
// MARK: Initialization, awakeFromNib, etc
//
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setup];
}

- (void)setup
{
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [[self firstPlayerScoreLabel] setBackgroundColor:[UIColor clearColor]];
    [[self firstPlayerScoreLabel] setTextColor:[UIColor whiteColor]];
    [[[self firstPlayerScoreLabel] superview] setBackgroundColor:[[self colors] objectAtIndex:UMFirstPlayer]];
    [[[[self firstPlayerScoreLabel] superview] layer] setCornerRadius:[[[self firstPlayerScoreLabel] superview] bounds].size.height / 2.0f];
    [[[self firstPlayerScoreLabel] superview] setClipsToBounds:YES];
    
    [[self secondPlayerScoreLabel] setBackgroundColor:[UIColor clearColor]];
    [[self secondPlayerScoreLabel] setTextColor:[UIColor whiteColor]];
    [[[self secondPlayerScoreLabel] superview] setBackgroundColor:[[self colors] objectAtIndex:UMSecondPlayer]];
    [[[[self secondPlayerScoreLabel] superview] layer] setCornerRadius:[[[self secondPlayerScoreLabel] superview] bounds].size.height / 2.0f];
    [[self secondPlayerScoreLabel] setClipsToBounds:YES];
}

//
// MARK: Delegate
//
@synthesize dataSource = _dataSource;

//
// MARK: Public methods
//
- (int)indexOfSquareInBoardWithLocation:(CGPoint)location
{
    int loc = -1;
    
    /*
     * Let's go through each square of the board and find the square that the given
     * location point belongs to.
     */
    for (int i = 0; i < [[self squaresBoard] count]; i++) {
        if (CGRectContainsPoint([[[self squaresBoard] objectAtIndex:i] CGRectValue], location)) {
            loc = i;
            break;
        }
    }
    
    return loc;
}  

- (BOOL)isPointInBoard:(CGPoint)location
{
    return CGRectContainsPoint(_boardRect, location);
}

- (BOOL)isPointInFirstPlayerScoreLabel:(CGPoint)location
{
    return CGRectContainsPoint([[[self firstPlayerScoreLabel] superview] frame], location);
}

- (BOOL)isPointInSecondPlayerScoreLabel:(CGPoint)location
{
    return CGRectContainsPoint([[[self secondPlayerScoreLabel] superview] frame], location);
}

- (void)showAliases
{
    NSString *firstPlayerAlias  = @"";
    NSString *secondPlayerAlias = @"";
    switch ([[self dataSource] mode:self]) {
        case UMOnePlayer:
            firstPlayerAlias  = [[self dataSource] firstPlayerAlias:self];
            secondPlayerAlias = @"AI";
            break;
            
        case UMTwoPlayers:
            firstPlayerAlias  = @"ME";
            secondPlayerAlias = @"U";
            break;
            
        case UMNetwork:
            firstPlayerAlias  = [[self dataSource] firstPlayerAlias:self];
            secondPlayerAlias = [[self dataSource] secondPlayerAlias:self];
            break;
            
        default:
            break;
    }
    
    NSString *firstPlayer = firstPlayerAlias;
    if ([[self dataSource] firstPlayerOutcome:self] != UMNone) {
        firstPlayer = [NSString stringWithFormat:@"%@\n%@", firstPlayerAlias, [self descriptionForOutcome:[[self dataSource] firstPlayerOutcome:self]]];
    }
    [[self firstPlayerLabel] setText:firstPlayer];

    NSString *secondPlayer = secondPlayerAlias;
    if ([[self dataSource] secondPlayerOutcome:self] != UMNone) {
        secondPlayer = [NSString stringWithFormat:@"%@\n%@", secondPlayerAlias, [self descriptionForOutcome:[[self dataSource] secondPlayerOutcome:self]]];
    }
    [[self secondPlayerLabel] setText:secondPlayer];
}

- (void)showInitialAliasesAndScores
{
    switch ([[self dataSource] mode:self]) {
        case UMOnePlayer:
            [[self firstPlayerLabel] setText:[[self dataSource] firstPlayerAlias:self]];
            [[self secondPlayerLabel] setText:@""];
            [[self firstPlayerScoreLabel] setText:@"ME"];
            [[self secondPlayerScoreLabel] setText:@"AI"];
            break;
            
        case UMTwoPlayers:
            [[self firstPlayerLabel] setText:@""];
            [[self secondPlayerLabel] setText:@""];
            [[self firstPlayerScoreLabel] setText:@"ME"];
            [[self secondPlayerScoreLabel] setText:@"U"];
            break;
            
        case UMNetwork:
            [[self firstPlayerLabel] setText:[[self dataSource] firstPlayerAlias:self]];
            [[self secondPlayerLabel] setText:[[self dataSource] secondPlayerAlias:self]];
            [[self firstPlayerScoreLabel] setText:@"GK"];
            [[self secondPlayerScoreLabel] setText:@"GK"];
            break;
            
        default:
            break;
    }
}

- (void)showWaitingForOpponentIfNeeded
{
    if ([[self dataSource] gameState:self] != UMGameIdle) {
        [[self waitingForOpponentLabel] setHidden:YES];
        return;
    }
    
    [[self waitingForOpponentLabel] setHidden:NO];
    [[self waitingForOpponentLabel] setAlpha:0.8];
    [UIView animateWithDuration:0.7
                          delay:1.0
                        options:UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse
                     animations:^{ [[self waitingForOpponentLabel] setAlpha:0.1]; }
                     completion:nil];
}

- (void)switchSecondPlayer {
    [[self secondPlayerScoreLabel] setText:[[self dataSource] mode:self] == UMTwoPlayers ? @"U" : @"AI"];
}

/* ------------------------------------------------------------------------------------------------
   squaresBoard: This property contains the definition of the 64 squares that make up the game
   board.
   ------------------------------------------------------------------------------------------------ */
@synthesize squaresBoard = _squaresBoard;
- (NSArray *)squaresBoard
{
    if (!_squaresBoard) {
        NSMutableArray *squares = [NSMutableArray array];
        
        CGFloat x = [self boardRect].origin.x;
        CGFloat y = [self boardRect].origin.y;
        CGFloat w = [self boardRect].size.width / UMCols;
        CGFloat h = [self boardRect].size.height / UMRows;
        
        for (int i = 0; i < UMRows; i++) {
            for (int j = 0; j < UMCols; j++) {
                [squares addObject:[NSValue valueWithCGRect:CGRectMake(x, y, w, h)]];
                x += w;
            }
            y += h;
            x = [self boardRect].origin.x;
        }
        
        _squaresBoard = [NSArray arrayWithArray:[squares copy]];
    }
    
    return _squaresBoard;
}


/* ------------------------------------------------------------------------------------------------
   scoreBoard: This property contains the rects the conform the score board drawing.                
   ------------------------------------------------------------------------------------------------ */
#define SPACE_AROUND_NAME        5
@synthesize scoreBoard   = _scoreBoard;
- (NSArray *)scoreBoard
{
    if (!_scoreBoard) {
        NSMutableArray *rects = [NSMutableArray array];
        
        CGRect player1TopRect = CGRectMake(60.0, 15.0, 60.0, 60.0);
        CGRect player2TopRect = CGRectMake(190.0, 15.0, 60.0, 60.0);
        
        [rects addObject:[NSValue valueWithCGRect:player1TopRect]];
        [rects addObject:[NSValue valueWithCGRect:player2TopRect]];
        
        _scoreBoard = [rects copy];
    }

    return _scoreBoard;
}


/* ------------------------------------------------------------------------------------------------
   boardRect: This property contains the game board inside the view.
   ------------------------------------------------------------------------------------------------ */
#define BOARD_YOFFSET 50.0
@synthesize boardRect = _boardRect;
- (CGRect)boardRect
{
    if (CGRectIsEmpty(_boardRect)) {        
        /*
         * We use a scale to define the size of the rectangle base on the width of the view.
         */
        _boardRect.size.width   = [self bounds].size.width * [self scale];
        _boardRect.size.height  = _boardRect.size.width;
        _boardRect.origin.x     = [self bounds].size.width / 2 - _boardRect.size.width / 2;
        _boardRect.origin.y     = [self bounds].size.height - _boardRect.size.height - [self safeAreaInsets].bottom;
    }
    
    return _boardRect;
}


/* ------------------------------------------------------------------------------------------------
   scale: This property contains scale in which the size of the board is calculated.
   ------------------------------------------------------------------------------------------------ */
@synthesize scale = _scale;
- (CGFloat)scale
{
    if (!_scale)
        _scale = 1.0;//.9;
    
    return _scale;
}


@synthesize verticalMaskView = _verticalMaskView;
- (UIView *)verticalMaskView
{
    if (!_verticalMaskView) {
        _verticalMaskView = [[UIView alloc] initWithFrame:CGRectNull];
        [_verticalMaskView setBackgroundColor:[UIColor grayColor]];
        [_verticalMaskView setAlpha:0.2];
        [self addSubview:_verticalMaskView];
    }
    
    return _verticalMaskView;
}

@synthesize horizontalMaskView = _horizontalMaskView;
- (UIView *)horizontalMaskView
{
    if (!_horizontalMaskView) {
        _horizontalMaskView = [[UIView alloc] initWithFrame:CGRectNull];
        [_horizontalMaskView setBackgroundColor:[UIColor grayColor]];
        [_horizontalMaskView setAlpha:0.2];
        [self addSubview:_horizontalMaskView];
    }
    
    return _horizontalMaskView;
}

@synthesize finalScoreLabel = _finalScoreLabel;
- (UILabel *)finalScoreLabel
{
    if (!_finalScoreLabel) {
        CGRect rect;
        rect.size.width  = [self boardRect].size.width * 0.60;
        rect.size.height = rect.size.width;
        rect.origin.x    = [self boardRect].size.width / 2 - rect.size.width / 2;
        rect.origin.y    = [self boardRect].origin.y + [self boardRect].size.height / 2 - rect.size.height / 2;
        _finalScoreLabel = [[UILabel alloc] initWithFrame:rect];
        [_finalScoreLabel setContentMode:UIViewContentModeRedraw];
        [_finalScoreLabel setTextAlignment:NSTextAlignmentCenter];
        [_finalScoreLabel setTextColor:[UIColor whiteColor]];
        [_finalScoreLabel setFont:[UIFont fontWithName:@"ArialRoundedMTBold" size:100.0]];
        [_finalScoreLabel setMinimumScaleFactor:0.5];
        [_finalScoreLabel setOpaque:NO];
        [_finalScoreLabel setBackgroundColor:[[UIColor darkGrayColor] colorWithAlphaComponent:0.6]];
        [[_finalScoreLabel layer] setCornerRadius:rect.size.height/2];
        [[_finalScoreLabel layer] setBorderWidth:1.0];
        [[_finalScoreLabel layer] setBorderColor:[[UIColor whiteColor] CGColor]];
        [_finalScoreLabel setClipsToBounds:YES];
        
        [self addSubview:_finalScoreLabel];
    }
    
    return _finalScoreLabel;
}

@synthesize viewFont = _viewFont;
- (UIFont *)viewFont
{
    if (!_viewFont)
        _viewFont = [UIFont fontWithName:@"ArialRoundedMTBold" size:22.0];
    
    return _viewFont;
}

@synthesize viewStringAttributes = _viewStringAttributes;
- (NSDictionary *)viewStringAttributes
{
    if (!_viewStringAttributes)
        _viewStringAttributes = @{NSFontAttributeName: [self viewFont]};
        
    return _viewStringAttributes;
}

@synthesize colors = _colors;
- (NSArray *)colors
{
    _colors = nil;
    
    switch ([[self dataSource] theme:self]) {
        case UMThemeClassic:
            return @[UIColorFromRGB(0x1870EC), UIColorFromRGB(0xE51C15), UIColorFromRGB(0xFBA617)];
            
        case UMThemeModern:
            return @[UIColorFromRGB(0x8F00FF), UIColorFromRGB(0xE85CFF), UIColorFromRGB(0x0B7DFD)];
            
        case UMThemeEco:
            return @[UIColorFromRGB(0x3E7300), UIColorFromRGB(0x79CF19), UIColorFromRGB(0xFBE500)];
            
        default:
            return @[UIColorFromRGB(0x1870EC), UIColorFromRGB(0xE51C15), UIColorFromRGB(0xFBA617)];
    }
}

@synthesize turnView = _turnView;
- (UIView *)turnView
{
    if (!_turnView) {
        _turnView = [[CircleView alloc] initWithFrame:CGRectZero];
        CGRect rect = CGRectMake(0.0f, 0.0f, 16.0f, 4.0f);
        [_turnView setFrame:rect];
        [[self turnContainerView] addSubview:_turnView];
        [[self turnContainerView] setBackgroundColor:[UIColor clearColor]];
    }
    
    return _turnView;
}

//
// MARK: View drawing
//

/* ------------------------------------------------------------------------------------------------
   drawRect: This methods is called by the system everytime it need to update the screen.
   ------------------------------------------------------------------------------------------------ */
- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    /*
     * Let's draw the board, its values, as well as the score board.
     */
    [self drawBoardWithContext:context];
    [self drawValuesOnBoardWithContext:context];
    [self drawHelperMaskWithContext:context];
    [self drawPlayerTurnIndicatorWithContext:context];
    
    [self showWaitingForOpponentIfNeeded];
    
    /*
     * Let's display the players alias and scores.
     */
    if ([[self dataSource] isFirstMove:self]) {
        [self showInitialAliasesAndScores];
    } else {
        [self showAliases];
        /*
         * Now we draw the score of each player.
         */
        if ([[self dataSource] mode:self] == UMOnePlayer || [[self dataSource] mode:self] == UMNetwork) {
            [self drawScore:[[self dataSource] firstPlayerPoints:self]
                     player:UMFirstPlayer withContext:context];
            [self drawScore:[[self dataSource] secondPlayerPoints:self]
                     player:UMSecondPlayer withContext:context];
        } else if ([[self dataSource] whoIsPlaying:self] == UMSecondPlayer) {
            [self drawScore:[[self dataSource] firstPlayerPoints:self]
                     player:UMFirstPlayer withContext:context];
        } else if ([[self dataSource] whoIsPlaying:self] == UMFirstPlayer) {
            [self drawScore:[[self dataSource] secondPlayerPoints:self]
                     player:UMSecondPlayer withContext:context];
        }
    }
    
    if ([[self dataSource] gameState:self] == UMGameOver)
        [self drawWinnerBox:[[self dataSource] winner:self] withContext:context];
    else
        [[self finalScoreLabel] setAlpha:0.0];
}


/* ------------------------------------------------------------------------------------------------
   drawBoardWithContext: This methods draws the board on the screen. It doesn't fill any values on
   it.

   Parameters:
   (CGContextRef)context: The graphic context to work with.
   ------------------------------------------------------------------------------------------------ */
- (void)drawBoardWithContext:(CGContextRef)context
{
    CGContextSaveGState(context);
    
    CGContextSetLineWidth(context, 1.0);
    
    CGContextSetStrokeColorWithColor(context, UIColorFromRGBA(0xc0c0c0, 0.5).CGColor);
    
    for (int i = 0; i < [[self squaresBoard] count]; i++) {
        CGContextAddRect(context, [[[self squaresBoard] objectAtIndex:i] CGRectValue]);
    }   
    CGContextStrokePath(context);
    
    CGContextRestoreGState(context);
}


/* ------------------------------------------------------------------------------------------------
   drawValuesOnBoardWithContext: This method fills the board with random number provided by the 
   dataSource.
 
   Parameters:
   (CGContextRef)context: The graphic context to work with.
   ------------------------------------------------------------------------------------------------ */
- (void)drawValuesOnBoardWithContext:(CGContextRef)context
{
    /* 
     * Let's go through each square on the board
     */
    int x = 1, y = 1;
    
    for (int i = 0; i < [[self squaresBoard] count]; i++) {
        CGRect square = [[[self squaresBoard] objectAtIndex:i] CGRectValue];
        
        
        if (((x + y) % 2) == 0) {
            CGContextSetRGBFillColor(context, 1.0, 1.0, 1.0, 0.50);
        } else {
            CGContextSetRGBFillColor(context, 0.95, 0.95, 0.95, 0.50);
        }
        
        y++;
        
        if (y > 8) {
            y = 1;
            x++;
        }

        
        /*
         * Let's obtain the value for the square where we are at. This is provided by the DataSource.
         */
        id value = [[[self dataSource] boardSquaresValue:self] objectAtIndex:i];
        if ([value isKindOfClass:[NSNumber class]]) {
            /* 
             * When the value is a number, it means that the square has not been played yet and we
             * proceed to display it in the square. 
             */
            CGRect textRect;
            
            textRect.size = [[value stringValue] sizeWithAttributes:[self viewStringAttributes]];
            textRect.origin.x = square.origin.x + square.size.width / 2 - textRect.size.width / 2;
            textRect.origin.y = square.origin.y + square.size.height / 2 - textRect.size.height / 2;
            CGContextFillRect(context, CGRectInset([[[self squaresBoard] objectAtIndex:i] CGRectValue], 0.0, 0.0));
            
            CGContextSetRGBFillColor(context, 0.20, 0.20, 0.20, 1.0);
            [[value stringValue] drawInRect:textRect withAttributes:[self viewStringAttributes]];
        } else if ([value isKindOfClass:[NSString class]]) {
            /* 
             * However, the value can also be a string that represent that the square has already been
             * selected and by whom (UMMe, UMIPhone, UMNone: initial cursor position).
             */
            CGContextSaveGState(context);
            
            /*
             * We fill the square with a specific color depending of who has selected. With the special
             * case of the intial cursor position which we fill it black.
             */
            if ([value isEqualToString:UMFirstPlayerKey])
                CGContextSetFillColorWithColor(context, [[[self colors] objectAtIndex:UMFirstPlayer] CGColor]);
            else if ([value isEqualToString:UMSecondPlayerKey])
                CGContextSetFillColorWithColor(context, [[[self colors] objectAtIndex:UMSecondPlayer] CGColor]);
            else
                CGContextSetRGBFillColor(context, 0.35, 0.35, 0.35, 1.0);
            

            CGContextFillRect(context, CGRectInset([[[self squaresBoard] objectAtIndex:i] CGRectValue], 0.5, 0.5));
            
            /*
             * Let's draw and fill a circle in the current cursor position to hint the player.
             */
            if (i == [[self dataSource] cursor:self]) {
                CGContextSetFillColorWithColor(context, [[[self colors] objectAtIndex:2] CGColor]);
                
                CGRect square = [[[self squaresBoard] objectAtIndex:i] CGRectValue];
                
                CGContextAddArc(context,
                                square.origin.x + square.size.width / 2, 
                                square.origin.y + square.size.height / 2, 
                                (square.size.width / 2) - 1,
                                0, 
                                2 * M_PI, 
                                YES);
                
                CGContextFillPath(context);
            }
            
            CGContextRestoreGState(context);
        }
       
    }
}

CGRect rectFor1PxStroke(CGRect rect)
{
    return CGRectMake(rect.origin.x + 0.5, rect.origin.y + 0.5, rect.size.width - 1, rect.size.height - 1);
}

void drawLinearGradient(CGContextRef context, CGRect rect, CGColorRef startColor, CGColorRef  endColor)
{
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGFloat locations[] = { 0.0, 1.0 };
    
    NSArray *colors = [NSArray arrayWithObjects:(__bridge id)startColor, (__bridge id)endColor, nil];
    
    CGGradientRef gradient = CGGradientCreateWithColors(colorSpace, (__bridge CFArrayRef) colors, locations);
    
    CGPoint startPoint = CGPointMake(CGRectGetMidX(rect), CGRectGetMinY(rect));
    CGPoint endPoint = CGPointMake(CGRectGetMidX(rect), CGRectGetMaxY(rect));
    
    CGContextSaveGState(context);
    CGContextAddRect(context, rect);
    CGContextClip(context);
    CGContextDrawLinearGradient(context, gradient, startPoint, endPoint, 0);
    CGContextRestoreGState(context);
    
    CGGradientRelease(gradient);
    CGColorSpaceRelease(colorSpace);
}

void draw1PxStroke(CGContextRef context, CGPoint startPoint, CGPoint endPoint, CGColorRef color)
{
    CGContextSaveGState(context);
    CGContextSetLineCap(context, kCGLineCapSquare);
    CGContextSetStrokeColorWithColor(context, color);
    CGContextSetLineWidth(context, 1.0);
    CGContextMoveToPoint(context, startPoint.x + 0.5, startPoint.y + 0.5);
    CGContextAddLineToPoint(context, endPoint.x + 0.5, endPoint.y + 0.5);
    CGContextStrokePath(context);
    CGContextRestoreGState(context);
}


/* ------------------------------------------------------------------------------------------------
   drawScore: This methods draws the score of one player on the screen.
 
   Parameters:
   (NSString *)score: The score of the player we want to display on the screen.
   (CGRect)rect: The area in the screen where we want to display the score.
   (CGContextRef)context: The graphic context to work on it.
   ------------------------------------------------------------------------------------------------ */
#define SCORE_OFFSET 70.0
- (void)drawScore:(NSString *)score inRect:(CGRect)rect withContext:(CGContextRef)context
{
    CGContextSaveGState(context);
    
    CGRect textRect;
    textRect.size     = [score sizeWithAttributes:[self viewStringAttributes]];
    textRect.origin.x = rect.origin.x + rect.size.width / 2 - textRect.size.width / 2;
    textRect.origin.y = rect.origin.y + rect.size.height / 2 - textRect.size.height / 2;
    
    CGContextSetRGBFillColor(context, 1.0, 1.0, 1.0, 1.0);
   
    [score drawInRect:textRect withAttributes:[self viewStringAttributes]];
    
    CGContextRestoreGState(context);
}

- (void)drawScore:(NSInteger)score player:(UMPlayer)player withContext:(CGContextRef)context
{
    
    if ([[self dataSource] isValidMove:self] == YES || [[self dataSource] isFirstMove:self] || [[self dataSource] mode:self] == UMNetwork) {
    
        if (player == UMFirstPlayer) {
            [UIView animateWithDuration:0.3 animations:^{
                [[[self firstPlayerScoreLabel] superview] setAlpha:0.7];
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:0.3 animations:^{
                    [[self firstPlayerScoreLabel] setText:[NSString stringWithFormat:@"%02ld", (long)score]];
                    [[[self firstPlayerScoreLabel] superview] setAlpha:1.0];
                }];
            }];
        } else if (player == UMSecondPlayer) {
            CGFloat delay = [[self dataSource] mode:self] == UMTwoPlayers ? 0.0 : 0.6;
            [UIView animateWithDuration:0.2 delay:delay options:UIViewAnimationOptionCurveLinear animations:^{
                [[[self secondPlayerScoreLabel] superview] setAlpha:0.7];
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:0.2 animations:^{
                    [[self secondPlayerScoreLabel] setText:[NSString stringWithFormat:@"%02ld", (long)score]];
                    [[[self secondPlayerScoreLabel] superview] setAlpha:1.0];
                }];
            }];
        }
        
    }
}


/* ------------------------------------------------------------------------------------------------
   drawPlayer: This methods draws the name of one player on the screen. 
 
   Parameters:
   (NSString *)name: The name of the player we want to display on the screen.
   (CGRect)rect: The area in the screen where we want to display the name.
   (CGContextRef)context: The graphic context to work on it.
   ------------------------------------------------------------------------------------------------ */
#define PLAYER_OFFSET 10.0
- (void)drawPlayer:(NSString *)name inRect:(CGRect)rect withContext:(CGContextRef)context
{
    CGContextSaveGState(context);
    
    CGRect textRect;
    textRect.size     = [name sizeWithAttributes:[self viewStringAttributes]];
    textRect.origin.x = rect.origin.x ;
    textRect.origin.y = (rect.origin.y + rect.size.height) - PLAYER_OFFSET;
    [[UIColor whiteColor] set];
    [name drawInRect:textRect withAttributes:[self viewStringAttributes]];
    
    CGContextRestoreGState(context);
}


/* ------------------------------------------------------------------------------------------------
   drawWinnerBox: When there is a winner this methods showes a Win o Loose message to the player.
 
   Parameters:
   (UMPlayer)winner: The winner of the game if there is one.
   (CGContextRef)context: The current graphic context to work on it.
   ------------------------------------------------------------------------------------------------ */
- (void)drawWinnerBox:(UMPlayer)winner withContext:(CGContextRef)context
{    
    UIGraphicsPushContext(context);
    CGContextSaveGState(context);
    
    [[self verticalMaskView] setHidden:YES];
    [[self horizontalMaskView] setHidden:YES];
    
    NSString *text = nil;
    UIColor *color = nil;
    if ([[self dataSource] firstPlayerOutcome:self] == UMOutcomeQuit) {
        text = @"Q";
        color = [[self colors] objectAtIndex:UMFirstPlayer];
    } else if ([[self dataSource] secondPlayerOutcome:self] == UMOutcomeQuit) {
        text = @"Q";
        color = [[self colors] objectAtIndex:UMSecondPlayer];
    } else {
        NSInteger score = labs([[self dataSource] firstPlayerPoints:self] - [[self dataSource] secondPlayerPoints:self]);
        text = score == 0 ? @"T" : [NSString stringWithFormat:@"%ld", (long)score];
        color = [[self colors] objectAtIndex:winner];
    }
    
    [[self finalScoreLabel] setText:text];
    [[self finalScoreLabel] setBackgroundColor:color];
    [[self finalScoreLabel] setTransform:CGAffineTransformMakeScale(0.25, 0.25)];

    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         [[self finalScoreLabel] setTransform:CGAffineTransformMakeScale(1.25, 1.25)];
                         [[self finalScoreLabel] setAlpha:1.0];
                     }
                     completion:^(BOOL finished) {
                         [UIView animateWithDuration:0.3 animations:^{
                             [[self finalScoreLabel] setTransform:CGAffineTransformMakeScale(1.0, 1.0)];
                         } completion:^(BOOL finished) {
                             [[self dataSource] didPresentedFinalScore:self];
                         }];
                     }];
     
    
    return;
}

- (void)drawPlayerTurnIndicatorWithContext:(CGContextRef)context
{
    UIGraphicsPushContext(context);
    CGContextSaveGState(context);
    
    CGRect  leftRect       = CGRectMake(0.0f, 0.0f, 16.0f, 4.0f);
    CGFloat containerWidth = [self turnContainerView].bounds.size.width;
    CGRect  rightRect      = CGRectMake(containerWidth - 16.0f, 0.0f, 16.0f, 4.0f);
    
    if ([[self dataSource] whoIsPlaying:self] == UMFirstPlayer) {
        [UIView animateWithDuration:0.3 animations:^{
            [[self turnView] setFrame:leftRect];
            [[self turnView] setBackgroundColor:[[self colors] objectAtIndex:UMFirstPlayer]];
        }];
    } else {
        [UIView animateWithDuration:0.3 animations:^{
            [[self turnView] setFrame:rightRect];
            [[self turnView] setBackgroundColor:[[self colors] objectAtIndex:UMSecondPlayer]];
        }];
    }
    
    CGContextRestoreGState(context);
    UIGraphicsPopContext();
}

- (void)drawHelperMaskWithContext:(CGContextRef)context
{
    UIGraphicsPushContext(context);
    CGContextSaveGState(context);
    
    int i = [[self dataSource] cursor:self];
    CGRect cursorRect = [[[self squaresBoard] objectAtIndex:i] CGRectValue];

    CGRect upperLeftSquareRect  = [[[self squaresBoard] objectAtIndex:0] CGRectValue];
    CGRect lowerRightSquareRect = [[[self squaresBoard] objectAtIndex:63] CGRectValue];
    
    CGRect verticalRect = CGRectMake(cursorRect.origin.x,
                                     upperLeftSquareRect.origin.y,
                                     cursorRect.size.width,
                                     lowerRightSquareRect.origin.y - upperLeftSquareRect.origin.y + lowerRightSquareRect.size.height);
    CGRect horizontalRect = CGRectMake(upperLeftSquareRect.origin.x,
                                       cursorRect.origin.y,
                                       lowerRightSquareRect.origin.x + lowerRightSquareRect.size.width,
                                       cursorRect.size.height);
    
    static int pastCursor     = -1;
    CGRect verticalFromRect   = verticalRect;
    CGRect horizontalFromRect = horizontalRect;
    
    if (pastCursor != -1) {
        cursorRect = [[[self squaresBoard] objectAtIndex:pastCursor] CGRectValue];
        verticalFromRect = CGRectMake(cursorRect.origin.x,
                                      upperLeftSquareRect.origin.y,
                                      cursorRect.size.width,
                                      lowerRightSquareRect.origin.y - upperLeftSquareRect.origin.y + lowerRightSquareRect.size.height);
        horizontalFromRect = CGRectMake(upperLeftSquareRect.origin.x,
                                        cursorRect.origin.y,
                                        lowerRightSquareRect.origin.x + lowerRightSquareRect.size.width,
                                        cursorRect.size.height);
    }
    pastCursor = i;

    if ([[self dataSource] isFirstMove:self]) {
        
        [[self verticalMaskView] setFrame:verticalRect];
        [[self verticalMaskView] setBackgroundColor:[UIColor grayColor]];
        [[self verticalMaskView] setHidden:NO];
        [[self horizontalMaskView] setFrame:horizontalRect];
        [[self horizontalMaskView] setBackgroundColor:[UIColor grayColor]];
        [[self horizontalMaskView] setHidden:NO];
    
    } else if ([[self dataSource] whoIsPlaying:self] == UMFirstPlayer &&
               [[self dataSource] myDirection:self] == UMHorizontal) {
                   
        [[self horizontalMaskView] setFrame:horizontalFromRect];
        [[self horizontalMaskView] setBackgroundColor:[[self colors] objectAtIndex:UMFirstPlayer]];
        [UIView animateWithDuration:0.3 animations:^{
            [[self horizontalMaskView] setFrame:horizontalRect];
        }];
        [[self verticalMaskView] setHidden:YES];
        [[self horizontalMaskView] setHidden:NO];
                   
    } else if ([[self dataSource] whoIsPlaying:self] == UMFirstPlayer &&
                [[self dataSource] myDirection:self] == UMVertical) {
                   
        [[self verticalMaskView] setFrame:verticalFromRect];
        [[self verticalMaskView] setBackgroundColor:[[self colors] objectAtIndex:UMFirstPlayer]];
        [UIView animateWithDuration:0.3 animations:^{
            [[self verticalMaskView] setFrame:verticalRect];
        }];
        [[self horizontalMaskView] setHidden:YES];
        [[self verticalMaskView] setHidden:NO];
    
    } else if ([[self dataSource] whoIsPlaying:self] == UMSecondPlayer &&
               [[self dataSource] myDirection:self] == UMHorizontal) {
        
        [[self verticalMaskView] setFrame:verticalFromRect];
        [[self verticalMaskView] setBackgroundColor:[[self colors] objectAtIndex:UMSecondPlayer]];
        [UIView animateWithDuration:0.3 animations:^{
            [[self verticalMaskView] setFrame:verticalRect];
        }];
        [[self horizontalMaskView] setHidden:YES];
        [[self verticalMaskView] setHidden:NO];
        
    } else if ([[self dataSource] whoIsPlaying:self] == UMSecondPlayer &&
               [[self dataSource] myDirection:self] == UMVertical) {
        
        [[self horizontalMaskView] setFrame:horizontalFromRect];
        [[self horizontalMaskView] setBackgroundColor:[[self colors] objectAtIndex:UMSecondPlayer]];
        [UIView animateWithDuration:0.3 animations:^{
            [[self horizontalMaskView] setFrame:horizontalRect];
        }];
        [[self verticalMaskView] setHidden:YES];
        [[self horizontalMaskView] setHidden:NO];
        
    }
    
    CGContextRestoreGState(context);
    UIGraphicsPopContext();
}

//
// MARK: Utilities
//
- (NSString *)descriptionForOutcome:(UMOutcome)outcome
{
    switch (outcome) {
        case UMOutcomeWon:  return @"(Won)";
        case UMOutcomeLost: return @"(Lost)";
        case UMOutcomeTied: return @"(Tied)";
        case UMOutcomeQuit: return @"(Quit)";
        case UMOutcomeNone: return @"";
    }
    
    return @"";
}

@end
