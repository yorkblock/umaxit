//
//  Copyright (c) 2012 York Block. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UMaxIt.h"


@class UMaxItView;


/**
 * The delegate must implement the following methods. UMaxItView uses the information to show
 * the progress of the game.
 */
@protocol UMaxItViewDataSource <NSObject>

/**
 * This method informs to UMaxItView the score of each player.
 *
 * @return The scores of the players.
 */
- (NSArray *)playersScore:(UMaxItView *)sender;
- (NSString *)firstPlayerAlias:(UMaxItView *)sender;
- (NSInteger)firstPlayerPoints:(UMaxItView *)sender;
- (UMOutcome)firstPlayerOutcome:(UMaxItView *)sender;
- (NSString *)secondPlayerAlias:(UMaxItView *)sender;
- (NSInteger)secondPlayerPoints:(UMaxItView *)sender;
- (UMOutcome)secondPlayerOutcome:(UMaxItView *)sender;

/**
 * This method informs to UMaxItView the values in the board. A value can be a number
 * if the square has not been selected yet, it can be string indicating which player selected
 * the square, or a string indicating the initial position of the cursor.
 *
 * @return The value of each square that conforms the board.
 */
- (NSArray *)boardSquaresValue:(UMaxItView *)sender;

/**
 * This method informs to UMaxItView which player is in turn.
 *
 * @return The player in turn.
 */
- (UMPlayer)whoIsPlaying:(UMaxItView *)sender;

/**
 * This method informs to UMaxItView the location of the cursor.
 * @return An index indicating the current cursor location.
 */
- (int)cursor:(UMaxItView *)sender;

/**
 * This method informs to UMaxItView the winner (if there is one) of the game.
 *
 * @return The winner of the game.
 */
- (UMPlayer)winner:(UMaxItView *)sender;

/**
 * This method informs to UMaxItView the direction of the user. It can be rows or columns.
 *
 * @return The direction of the user.
 */
- (UMDirection)myDirection:(UMaxItView *)sender;

/**
 * This method informs to UMaxItView if it is the first move of the game.
 *
 * @return TRUE if it is the firs move, FALSE otherwise.
 */
- (BOOL)isFirstMove:(UMaxItView *)sender;

- (void)didPresentedFinalScore:(UMaxItView *)sender;

- (UMTheme)theme:(UMaxItView *)sender;

- (BOOL)isValidMove:(UMaxItView *)sender;

- (UMMode)mode:(UMaxItView *)sender;

- (UMGameState)gameState:(UMaxItView *)sender;

@end


@interface UMaxItView : UIView

/**
 * This method gives the square position given a point in the view.
 *
 * @param location a CGPoint in the view.
 * @return The square position (0..63) or -1 if location is none of the squares in the board.
 */
- (int)indexOfSquareInBoardWithLocation:(CGPoint)location;
- (BOOL)isPointInBoard:(CGPoint)location;
- (BOOL)isPointInFirstPlayerScoreLabel:(CGPoint)location;
- (BOOL)isPointInSecondPlayerScoreLabel:(CGPoint)location;

@property (nonatomic, weak) IBOutlet id <UMaxItViewDataSource> dataSource;

@end
