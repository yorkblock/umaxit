//
//  Copyright (c) 2012 York Block. All rights reserved.
//

#import <AudioToolbox/AudioServices.h>

#import "UMaxItBaseViewController.h"
#import "ScoresTableViewController.h"


@interface UMaxItBaseViewController () <UMaxItViewDataSource>

@end


@implementation UMaxItBaseViewController

//
// MARK: Synthesize private properties
//
@synthesize uMaxItView = _uMaxItView;
- (void)setUMaxItView:(UMaxItView *)uMaxItView
{
    _uMaxItView = uMaxItView;
    
    [_uMaxItView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(squareSelectionGesture:)]];
    [_uMaxItView setDataSource:self];
}


//
// MARK: Gestures methods
//
- (void)squareSelectionGesture:(UITapGestureRecognizer *)gesture
{
    if ([gesture state] != UIGestureRecognizerStateEnded)
        return;

    int i = -1;
    CGPoint point = [gesture locationInView:[self uMaxItView]];
    if ([[self gameModel] gameState] == UMGameStarted) {
        if ((i = [[self uMaxItView] indexOfSquareInBoardWithLocation:point]) >= 0) {
            [[self gameModel] playToPosition:UMIndexToSquare(i)];
            [[self uMaxItView] setNeedsDisplay];
            [self playFile:@"/touch-cell.aiff"];
        } else {
            [self playFile:@"/touch-fail.aiff"];
        }
    } else if ([[self gameModel] gameState] == UMGamePlaying) {
        if ((i = [[self uMaxItView] indexOfSquareInBoardWithLocation:point]) >= 0) {
            [[self gameModel] playToPosition:UMIndexToSquare(i)];
            [[self uMaxItView] setNeedsDisplay];
            [self playFile:@"/touch-cell.aiff"];
        } else {
            [self playFile:@"/touch-fail.aiff"];
        }
    } else
        return;
}


//
// MARK: Load, Unload, and such methods
//
- (void)viewDidLoad
{
    [super viewDidLoad];

    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 50.0, 30.0)];
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:@"Arial Rounded MT Bold" size:24],
                                 NSForegroundColorAttributeName: [UIColor lightGrayColor]};
    [titleLabel setAttributedText:[[NSAttributedString alloc] initWithString:@"UMaxIt"
                                                                  attributes:attributes]];
    [titleLabel setTextColor:[UIColor lightGrayColor]];
    [[self navigationItem] setTitleView:titleLabel];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"\uF060"
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:nil
                                                                  action:NULL];
    [backButton setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"FontAwesome" size:24],
                                         NSForegroundColorAttributeName: [UIColor lightGrayColor]}
                              forState:UIControlStateNormal];
    [backButton setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"FontAwesome" size:24],
                                         NSForegroundColorAttributeName: [UIColor grayColor]}
                              forState:UIControlStateHighlighted];
    [[self navigationItem] setBackBarButtonItem:backButton];
    
    [self setSettings:[[Settings alloc] init]];
    [[self settings] load];
}


//
// MARK: UMaxItViewDataSource implementation
//
- (NSArray *)playersScore:(UMaxItView *)sender
{
    return nil;
}

- (NSString *)firstPlayerAlias:(UMaxItView *)sender
{
    return [[self gameModel] firstPlayerAlias];
}

- (NSInteger)firstPlayerPoints:(UMaxItView *)sender
{
    return [[self gameModel] firstPlayerPoints];
}

- (UMOutcome)firstPlayerOutcome:(UMaxItView *)sender
{
    return [[self gameModel] firstPlayerOutcome];
}

- (NSString *)secondPlayerAlias:(UMaxItView *)sender
{
    return [[self gameModel] secondPlayerAlias];
}

- (NSInteger)secondPlayerPoints:(UMaxItView *)sender
{
    return [[self gameModel] secondPlayerPoints];
}

- (UMOutcome)secondPlayerOutcome:(UMaxItView *)sender
{
    return [[self gameModel] secondPlayerOutcome];
}

- (NSArray *)boardSquaresValue:(UMaxItView *)sender
{
    return [[self gameModel] board];    
}

- (UMPlayer)whoIsPlaying:(UMaxItView *)sender
{
    return [[self gameModel] playerTurn];
}

- (int)cursor:(UMaxItView *)sender
{
    return UMSquareToIndex([[self gameModel] cursorLocation]);
}

- (UMPlayer)winner:(UMaxItView *)sender
{
    if ([[self gameModel] isGameOver]) {
        return [[self gameModel] winner];
    }
    
    return UMNone;
}

- (UMDirection)myDirection:(UMaxItView *)sender
{
    return [[self gameModel] firstPlayerDirection];
}

- (BOOL)isFirstMove:(UMaxItView *)sender
{
    return [[self gameModel] gameState] == UMGameStarted || [[self gameModel] gameState] == UMGameIdle;
}

- (void)didPresentedFinalScore:(UMaxItView *)sender
{
}

- (UMTheme)theme:(UMaxItView *)sender
{
    return [[self settings] theme];
}

- (BOOL)isValidMove:(UMaxItView *)sender
{
    return [[self gameModel] isValidMove];
}

- (UMMode)mode:(UMaxItView *)sender
{
    return [[self gameModel] mode];
}

- (UMGameState)gameState:(UMaxItView *)sender
{
    return [[self gameModel] gameState];
}


//
// MARK: Utility methods
//
- (void)playFile:(NSString *)name
{
    if (![[self settings] isSoundOn])
        return;
    
	NSString *path = [NSString stringWithFormat:@"%@%@",
					  [[NSBundle mainBundle] resourcePath],
					  name];
    SystemSoundID soundID;
	
    NSURL *filePath = [NSURL fileURLWithPath:path isDirectory:NO];
	
	AudioServicesCreateSystemSoundID((__bridge CFURLRef)filePath, &soundID);
	
	AudioServicesPlaySystemSound(soundID);
}

@end
