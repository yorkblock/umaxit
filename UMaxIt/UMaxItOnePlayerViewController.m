//
//  Copyright (c) 2020 York Block. All rights reserved.
//

#import <GameKit/GameKit.h>

#import "UMaxItOnePlayerViewController.h"

static const int ddLogLevel = LOG_LEVEL_VERBOSE;


@interface UMaxItOnePlayerViewController ()

@end


@implementation UMaxItOnePlayerViewController

//
// MARK: Init methods
//
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self addTopBarButtons];
    [self reset];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reportScore:) name:UMGameDidStoreScoreNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UMGameDidStoreScoreNotification object:nil];
}

- (void)addTopBarButtons
{
    UIBarButtonItem *scores = [self buttonItemWithTitle:@"\uF012" selector:@selector(showScores)];
    UIBarButtonItem *reset = [self buttonItemWithTitle:@"\uF021" selector:@selector(reset)];
    
    [[self navigationItem] setRightBarButtonItems:@[reset, scores]];
}

- (void)reset
{
    [self setGameModel:[[UMaxItModel alloc] initWithMode:UMOnePlayer]];
    [[self gameModel] updateLocalPlayerId:[[GKLocalPlayer localPlayer] playerID]
                                    alias:[[GKLocalPlayer localPlayer] alias]];
    [[self uMaxItView] setNeedsDisplay];
}

- (void)showScores
{
    [self performSegueWithIdentifier:@"ShowLocalScores" sender:self];
}

- (UIBarButtonItem *)buttonItemWithTitle:(NSString *)title selector:(SEL)selector
{
    UIBarButtonItem *buttonItem = [[UIBarButtonItem alloc] initWithTitle:title
                                                                   style:UIBarButtonItemStyleDone
                                                                  target:self action:selector];
    
    [buttonItem setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"FontAwesome" size:24],
                                         NSForegroundColorAttributeName: [UIColor lightGrayColor]}
                              forState:UIControlStateNormal];
    [buttonItem setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"FontAwesome" size:24],
                                         NSForegroundColorAttributeName: [UIColor grayColor]}
                              forState:UIControlStateHighlighted];
    
    return buttonItem;
}


//
// MARK: Report Score
//
- (void)reportScore:(NSNotification *)notification
{
    [self reportGKScore:[[self gameModel] score]];
}

- (void)reportGKScore:(NSNumber *)score
{
    GKScore *scoreReporter = [[GKScore alloc] initWithLeaderboardIdentifier:GameCenterLeaderboardHardCategory];
    [scoreReporter setValue:[score integerValue]];
    [scoreReporter setContext:0];
    
    [GKScore reportScores:@[scoreReporter] withCompletionHandler:^(NSError *error) {
        if (error) {
            DDLogError(@"GameCenter reportScore error (%ld: %@)", (long)[error code], [error userInfo]);
            return;
        }
        
        DDLogInfo(@"GameCenter reportScore successful");
    }];
}

@end
