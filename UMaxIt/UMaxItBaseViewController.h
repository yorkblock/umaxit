//
//  Copyright (c) 2012 York Block. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UMaxIt.h"
#import "UMaxItModel.h"
#import "UMaxItView.h"
#import "Settings.h"
#import "DDLog.h"


@interface UMaxItBaseViewController : UIViewController

@property (nonatomic, weak)   IBOutlet UMaxItView  *uMaxItView;
@property (nonatomic, strong)          UMaxItModel *gameModel;
@property (nonatomic, strong)          Settings    *settings;

@end
