//
//  Copyright (c) 2020 York Block. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UMaxItBaseViewController.h"


NS_ASSUME_NONNULL_BEGIN

@interface UMaxItTwoPlayersViewController : UMaxItBaseViewController

@end

NS_ASSUME_NONNULL_END
