//
//  Copyright (c) 2012 York Block. All rights reserved.
//

#ifndef UMaxIt_UMaxIt_h
#define UMaxIt_UMaxIt_h

#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define UIColorFromRGBA(rgbValue,a) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:a]

typedef struct UMSquare {
    unsigned int row;
    unsigned int col;
} UMSquare;

typedef enum UMBoard {
    UMRows = 8,
    UMCols = 8
} UMBoard;

typedef enum UMPlayer {
    UMFirstPlayer   =  0,
    UMSecondPlayer  =  1,
    UMNone          =  2,
} UMPlayer;

typedef enum UMOutcome {
    UMOutcomeNone,
    UMOutcomeWon,
    UMOutcomeTied,
    UMOutcomeLost,
    UMOutcomeQuit
} UMOutcome;

typedef enum UMDirection {
    UMHorizontal,
    UMVertical
} UMDirection;

typedef enum UMMode {
    UMOnePlayer,
    UMTwoPlayers,
    UMNetwork
} UMMode;

typedef enum UMGameState {
    UMGameIdle,
    UMGameStarted,
    UMGamePlaying,
    UMGameOver
} UMGameState;

typedef enum UMTheme {
    UMThemeClassic = 0,
    UMThemeModern,
    UMThemeEco
} UMTheme;

#define UMSquareToIndex(square)     (UMRows * (square.row) + (square.col))
#define UMIndexToSquare(index)      (UMSquare){(int)((index) / UMRows), (index) % UMCols}

static NSString * const UMNoneKey                         = @"UMNoneKey";
static NSString * const UMFirstPlayerKey                  = @"UMFirstPlayerKey";
static NSString * const UMSecondPlayerKey                 = @"UMSecondPlayerKey";
static NSString * const UMPlayersKey                      = @"UMPlayersKey";
static NSString * const UMIdKey                           = @"UMIdKey";
static NSString * const UMAliasKey                        = @"UMAliasKey";
static NSString * const UMPointsKey                       = @"UMPointsKey";
static NSString * const UMDirectionKey                    = @"UMDirectionKey";
static NSString * const UMOutcomeKey                      = @"UMOutcomeKey";
static NSString * const UMScoreKey                        = @"UMScoreKey";
static NSString * const UMBoardKey                        = @"UMBoardKey";
static NSString * const UMCursorLocationKey               = @"UMCursorLocationKey";
static NSString * const UMPlayerTurnKey                   = @"UMPlayerTurnKey";
static NSString * const UMModeKey                         = @"UMModeKey";
static NSString * const UMGameStateKey                    = @"UMGameStateKey";
static NSString * const UMDateKey                         = @"UMDateKey";
static NSString * const UMTopScoresPrefKey                = @"UMTopScoresPrefKey";
static NSString * const GameCenterLeaderboardHardCategory = @"com.blockembedded.umaxit.leaderboard.hard";

#endif
