//
//  Copyright (c) 2013 York Block. All rights reserved.
//

#import <AudioToolbox/AudioServices.h>
#import <GameKit/GameKit.h>

#import "MainMenuViewController.h"
#import "UMaxIt.h"
#import "UMaxItNetworkViewController.h"
#import "Settings.h"
#import "DDLog.h"


static const int ddLogLevel = LOG_LEVEL_VERBOSE;


@interface MainMenuViewController () <UINavigationControllerDelegate, GKLocalPlayerListener>

@property (nonatomic, weak)   IBOutlet UIButton                *onePlayerButton;
@property (nonatomic, weak)   IBOutlet UIButton                *twoPlayersButton;
@property (nonatomic, weak)   IBOutlet UIButton                *newtorkPlayersButton;
@property (nonatomic, weak)   IBOutlet UIActivityIndicatorView *activityIndicatorView;
@property (nonatomic, weak)   IBOutlet UIView                  *containerView;
@property (nonatomic, weak)   IBOutlet UILabel                 *copyrightLabel;

@property (nonatomic, strong)          Settings                *settings;

@end


@implementation MainMenuViewController 

//
// MARK: Actions and Segues methods
//
- (IBAction)playWithOnePlayer:(id)sender
{
    [self performSegueWithIdentifier:@"ShowOnePlayerGame" sender:self];
    [self playFile:@"/touch-button.aiff"];
}

- (IBAction)playWithTwoPlayer:(id)sender
{
    [self performSegueWithIdentifier:@"ShowTwoPlayersGame" sender:self];
    [self playFile:@"/touch-button.aiff"];
}

- (IBAction)playOverTheNetwork:(id)sender
{
    if ([[GKLocalPlayer localPlayer] isAuthenticated] == NO) {
        [self alertGameCenterRequired];
        return;
    }
    
    [self performSegueWithIdentifier:@"ShowNetworkGame" sender:nil];
    [self playFile:@"/touch-button.aiff"];
}

- (IBAction)showSettings:(id)sender
{
    [self performSegueWithIdentifier:@"ShowSettings" sender:self];
    [self playFile:@"/touch-button.aiff"];
}

- (IBAction)showHowToPlay:(id)sender
{
    [self performSegueWithIdentifier:@"ShowHowToPlay" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"ShowNetworkGame"]) {
        UMaxItNetworkViewController *vc = (UMaxItNetworkViewController *)[segue destinationViewController];
        [vc setMatch:sender];
    }
}


//
// MARK: Inits methods
//
- (void)awakeFromNib
{
    [super awakeFromNib];
    [[self navigationController] setDelegate:self];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    CGRect rect = [[self containerView] frame];
    rect.origin.y = ([[[self containerView] superview] frame].size.height - rect.size.height) / 2 - 10;
    [[self containerView] setFrame:rect];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if ([Settings isFirstLaunch]) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self showHowToPlay:nil];
        });
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    NSString *buildVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    NSString *buildNumber = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
    [[self copyrightLabel] setText:[@"©York Block + nube360 " stringByAppendingFormat:@"v%@(%@)", buildVersion, buildNumber]];
    
    [self setSettings:[[Settings alloc] init]];
    [[self settings] load];
        
    // Navigation Bar configuration
    [[[self navigationController] navigationBar] setBackIndicatorImage:[[UIImage alloc] init]];
    [[[self navigationController] navigationBar] setBackIndicatorTransitionMaskImage:[[UIImage alloc] init]];
    [[[self navigationController] navigationBar] setBarStyle:UIBarStyleBlackOpaque];
    [[[self navigationController] navigationBar] setTranslucent:NO];
    [[[self navigationController] navigationBar] setBarTintColor:[UIColor whiteColor]];
    
    // Navigation back button confguration
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"\uF060"
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:nil
                                                                  action:NULL];
    [backButton setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"FontAwesome" size:24],
                                         NSForegroundColorAttributeName: [UIColor lightGrayColor]}
                              forState:UIControlStateNormal];
    [backButton setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"FontAwesome" size:24],
                                         NSForegroundColorAttributeName: [UIColor grayColor]}
                              forState:UIControlStateHighlighted];

    [[self navigationItem] setBackBarButtonItem:backButton];
    
    [self authenticateLocalPlayer];
}


//
// MARK: UINavigationControllerDelegate
//
- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    if (viewController == [[navigationController viewControllers] objectAtIndex:0]) {
        [navigationController setNavigationBarHidden:YES];
    } else {
        [navigationController setNavigationBarHidden:NO];
    }
}


//
// MARK: GameCenter Auth
//
- (void)authenticateLocalPlayer
{
    [[self activityIndicatorView] startAnimating];
    [[self newtorkPlayersButton] setEnabled:NO];
    
    __weak MainMenuViewController *weakSelf = self;
    [[GKLocalPlayer localPlayer] setAuthenticateHandler:^(UIViewController *viewController, NSError *error) {
        [[weakSelf activityIndicatorView] stopAnimating];
        [[self newtorkPlayersButton] setEnabled:YES];

        if (error) {
            DDLogError(@"Player authenticate handler (%ld: %@)", (long)[error code], [error userInfo]);
            return;
        }
        
        if (viewController) {
            [weakSelf presentViewController:viewController animated:YES completion:nil];
        } else if ([[GKLocalPlayer localPlayer] isAuthenticated]) {
            DDLogInfo(@"Player auntheticaded (%@)", [GKLocalPlayer localPlayer]);
            [[GKLocalPlayer localPlayer] registerListener:self];
        } else {
            DDLogError(@"Player NO auntheticaded");
        }
    }];
}


//
// MARK: Listener methods for GKLocalPlayerListener
//
- (void)player:(GKPlayer *)player receivedTurnEventForMatch:(GKTurnBasedMatch *)match didBecomeActive:(BOOL)didBecomeActive
{
    DDLogVerbose(@"M ReceivedTurnEventForMatch active(%d)", didBecomeActive);
    
    if (didBecomeActive == NO)
        return;
    
    if ([[[self navigationController] childViewControllers] count] > 1)
        return;
    
    [self performSegueWithIdentifier:@"ShowNetworkGame" sender:match];
}


//
// MARK: Utilities
//
- (void)playFile:(NSString *)name
{
    if (![[self settings] isSoundOn])
        return;
    
    NSString *path = [NSString stringWithFormat:@"%@%@",
                      [[NSBundle mainBundle] resourcePath],
                      name];
    SystemSoundID soundID;
    
    NSURL *filePath = [NSURL fileURLWithPath:path isDirectory:NO];
    
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)filePath, &soundID);
    
    AudioServicesPlaySystemSound(soundID);
}

- (void)alertGameCenterRequired
{
    UIAlertController *ac = [UIAlertController alertControllerWithTitle:@"Error"
                                                                message:@"Please go to setting and login to Game Center"
                                                         preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK"
                                                     style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * _Nonnull action) {}];
    
    [ac addAction:action];
    [self presentViewController:ac animated:YES completion:nil];
}

@end
