//
//  Copyright (c) 2012 York Block. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UMaxIt.h"


FOUNDATION_EXPORT NSNotificationName const UMGameDidUpdateNotification;
FOUNDATION_EXPORT NSNotificationName const UMGameDidStoreScoreNotification;


@interface UMaxItModel : NSObject

/**
* Each game is contained in a NSDictionary with the following keys:
* - FirstPlayer
*      - Id
*      - Alias
*      - Points
*      - Direction
*      - Outcome
* - SecondPlayer
*      - Id
*      - Alias
*      - Points
*      - Direction
*      - Outcome
* - Score
* - Board
* - CursorLocation
* - PlayerTurn
* - Mode (one player, two players, network)
* - State
* - Date
*/
@property (nonatomic, strong)           NSMutableDictionary *game;

@property (nonatomic, readonly, weak)   NSString            *firstPlayerId;
@property (nonatomic, readonly, weak)   NSString            *firstPlayerAlias;
@property (nonatomic, readonly)         NSInteger            firstPlayerPoints;
@property (nonatomic, readonly)         UMDirection          firstPlayerDirection;
@property (nonatomic, readonly)         UMOutcome            firstPlayerOutcome;

@property (nonatomic, readonly, weak)   NSString            *secondPlayerId;
@property (nonatomic, readonly, weak)   NSString            *secondPlayerAlias;
@property (nonatomic, readonly)         NSInteger            secondPlayerPoints;
@property (nonatomic, readonly)         UMDirection          secondPlayerDirection;
@property (nonatomic, readonly)         UMOutcome            secondPlayerOutcome;

@property (nonatomic, readonly, weak)   NSMutableArray      *board;
@property (nonatomic, readonly)         UMGameState          gameState;
@property (nonatomic, readonly)         UMMode               mode;
@property (nonatomic, readonly)         UMSquare             cursorLocation;
@property (nonatomic, readonly)         UMPlayer             playerTurn;
@property (nonatomic, readonly)         UMPlayer             winner;
@property (nonatomic, readonly, weak)   NSString            *winnerId;
@property (nonatomic, readonly, weak)   NSNumber            *score;
@property (nonatomic, readonly)         BOOL                 isGameOver;
@property (nonatomic, readonly)         BOOL                 isValidMove;

@property (nonatomic, readonly)         BOOL                 isLocalPlayerQuitting;
@property (nonatomic, readonly)         BOOL                 isOpponentPlayerQuitting;
@property (nonatomic, readonly)         UMOutcome            localPlayerOutcome;
@property (nonatomic, readonly)         UMOutcome            opponentPlayerOutcome;

- (id)initWithMode:(UMMode)mode;
- (void)playToPosition:(UMSquare)square;
- (void)updateLocalPlayerId:(NSString *)iD alias:(NSString *)alias;
- (void)updatePlayersInfoOnCompletion:(void (^)(void))onCompletion;
- (void)endGameLocalPlayerOutcome:(UMOutcome)localOutcome opponentPlayerOutcome:(UMOutcome)opponentOutcome;

@end
