//
//  Copyright (c) 2012 York Block. All rights reserved.
//

#import <stdlib.h>
#import <time.h>

#import "UMaxItModel.h"
#import "DDLog.h"
#import "CoreFoundation/CoreFoundation.h"
#import "LocalScores.h"


static const int ddLogLevel = LOG_LEVEL_VERBOSE;

NSNotificationName const UMGameDidUpdateNotification     = @"UMGameDidUpdateNotification";
NSNotificationName const UMGameDidStoreScoreNotification = @"UMGameDidStoreScoreNotification";


@interface UMaxItModel ()

@property (nonatomic, strong) NSString  *localPlayerId;
@property (nonatomic, strong) NSString  *localPlayerAlias;

@end


@implementation UMaxItModel

@synthesize game             = _game;
@synthesize localPlayerId    = _localPlayerId;
@synthesize localPlayerAlias = _localPlayerAlias;

//
// MARK: Game core
//
- (void)playToPosition:(UMSquare)square
{
    switch ([self mode]) {
        case UMOnePlayer:
            [self playOnePlayerMoveToPosition:square];
            break;
            
        case UMTwoPlayers:
            [self playTwoPlayerMoveToPosition:square];
            break;
            
        case UMNetwork:
            [self playNetworkPlayerMoveToPosition:square];
            break;
            
        default:
            break;
    }
}

- (void)playOnePlayerMoveToPosition:(UMSquare)square
{
    if ([self isGameOver])
        return;
    
    if ([self isMyTurn]) {
        if ([self isGameOver]) {
            DDLogInfo(@"Game Over");
            return;
        }
        
        [self player:UMFirstPlayer moveToSquare:square];
        if ([self isMyTurn])
            return;
        
        if ([self isGameOver]) {
            DDLogInfo(@"Game Over");
            return;
        }
        
        [self iPhoneMove:[self mode]];
        if ([self isGameOver]) {
            DDLogInfo(@"Game Over");
            return;
        }
    }
}

- (void)playTwoPlayerMoveToPosition:(UMSquare)square
{
    if ([self isGameOver])
        return;
    
    UMPlayer player = [self playerTurn];
    [self player:player moveToSquare:square];
    if ([self isGameOver]) {
        DDLogInfo(@"Game Over");
        return;
    }
}

- (void)playNetworkPlayerMoveToPosition:(UMSquare)square
{
    if ([self isGameOver])
        return;
    
    if ([self isMyTurn] == NO)
        return;
    
    UMPlayer player = [self playerTurn];
    if ([self player:player moveToSquare:square] == NO)
        return;
    
    if ([self isGameOver])
        DDLogInfo(@"Game Over");
    
    [[NSNotificationCenter defaultCenter] postNotificationName:UMGameDidUpdateNotification object:self];
}

@synthesize isGameOver = _isGameOver;
- (BOOL)isGameOver
{
    if ([self gameState] == UMGameOver)
        return YES;
    
    BOOL isOver = YES;
    
    UMPlayer player = [self playerTurn];
    UMDirection direction = [self firstPlayerDirection];
    UMSquare location = [self cursorLocation];
    
    if (player == UMFirstPlayer) {
        if (direction == UMHorizontal) {
            for (int i = 0; i < UMCols; i++) {
                UMSquare square = {location.row, i};
                if ([self boardContainsNumberOnSquare:square])
                    isOver = NO;
            }
        } else {
            for (int i = 0; i < UMRows; i++) {
                UMSquare square = {i, location.col};
                if ([self boardContainsNumberOnSquare:square])
                    isOver = NO;
            }
        }
    } else {
        if (direction == UMHorizontal) {
            for (int i = 0; i < UMCols; i++) {
                UMSquare square = {i, location.col};
                if ([self boardContainsNumberOnSquare:square])
                    isOver = NO;
            }
        } else {
            for (int i = 0; i < UMCols; i++) {
                UMSquare square = {location.row, i};
                if ([self boardContainsNumberOnSquare:square])
                    isOver = NO;
            }
        }
    }
    
    if (isOver) {
        [self setGameState:UMGameOver];
        
        NSInteger score = [self firstPlayerPoints] - [self secondPlayerPoints];
        [self setScore:[NSNumber numberWithInteger:score]];

        if (score < 0) {
            [self setOutcome:UMOutcomeLost forPlayer:[self firstPlayer]];
            [self setOutcome:UMOutcomeWon forPlayer:[self secondPlayer]];
        } else if (score > 0) {
            [self setOutcome:UMOutcomeWon forPlayer:[self firstPlayer]];
            [self setOutcome:UMOutcomeLost forPlayer:[self secondPlayer]];
        } else {
            [self setOutcome:UMOutcomeTied forPlayer:[self firstPlayer]];
            [self setOutcome:UMOutcomeTied forPlayer:[self secondPlayer]];
        }
        
        [self reportGameScore];
    }
    
    return isOver;
}

- (NSArray *)fillSquares
{
    int possibleValues[] = {-9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
    NSMutableArray *board = [NSMutableArray array];
    
    for (int i = 0; i < UMRows; i++) {
        for (int j = 0; j < UMCols; j++) {
            int iRnd = (int)((double)rand() / ((double)RAND_MAX + 1) * sizeof(possibleValues) / sizeof(possibleValues[0]));
            [board addObject:[NSNumber numberWithInt:possibleValues[iRnd]]];
        }
    }
    
    return [board copy];
}

- (BOOL)isMyTurn
{
    if ([self mode] == UMNetwork) {
        if ([self playerTurn] == UMFirstPlayer &&
            [[self firstPlayerId] isEqualToString:[self localPlayerId]])
            return YES;
        else if ([self playerTurn] == UMSecondPlayer &&
            [[self secondPlayerId] isEqualToString:[self localPlayerId]])
            return YES;
        else
            return NO;
    }
    
    return  [self playerTurn] == UMFirstPlayer ? YES : NO;
}

@synthesize isValidMove = _isValidMove;
- (void)setIsValidMove:(BOOL)isValidMove
{
    _isValidMove = isValidMove;
}

- (BOOL)player:(UMPlayer)player moveToSquare:(UMSquare)square
{
    [self setIsValidMove:[self isPlayer:player moveToSquareValid:square]];
    if ([self isValidMove] == NO)
        return NO;
    
    [self setGameState:UMGamePlaying];
    [self setCursorLocation:square];
    [self addToPlayer:player pointsFromSquare:square];
    [self updateBoardInLocation:square];
    
    UMPlayer playerTurn = (player == UMFirstPlayer ? UMSecondPlayer : UMFirstPlayer);
    [self setPlayerTurn:playerTurn];

    [self save];
    
    return YES;
}

- (BOOL)isPlayer:(UMPlayer)player moveToSquareValid:(UMSquare)square
{
    BOOL isValid = YES;
    
    if (![self boardContainsNumberOnSquare:square])
        return NO;
    
    UMSquare location = [self cursorLocation];
    if ((location.row == square.row) && (location.col == square.col))
        return NO;
    
    NSMutableDictionary *moving =  [[self players] objectAtIndex:player];
    NSMutableDictionary *resting = player == UMFirstPlayer ? [self secondPlayer] : [self firstPlayer];
    
    if ([self gameState] == UMGameStarted) {
        
        if (square.col == location.col) {
            [self setDirection:UMVertical forPlayer:moving];
            [self setDirection:UMHorizontal forPlayer:resting];
        } else if (square.row == location.row) {
            [self setDirection:UMHorizontal forPlayer:moving];
            [self setDirection:UMVertical forPlayer:resting];
        } else {
            isValid = NO;
        }
        
    } else {
        
        UMDirection direction = [self directionForPlayerAt:player];
        if ((direction == UMVertical && square.col != location.col) ||
            (direction == UMHorizontal && square.row != location.row))
            isValid = NO;
        
    }
    
    return isValid;
}

- (void)addToPlayer:(UMPlayer)player pointsFromSquare:(UMSquare)square
{
    NSMutableDictionary *thePlayer = [self playerAt:player];
    NSInteger squarePoints = [self boardPointsOnSquare:square];
    NSInteger playerPoints = [self pointsForPlayer:thePlayer];
    
    [self setPoints:squarePoints + playerPoints forPlayer:thePlayer];
}

- (void)updateBoardInLocation:(UMSquare)location
{
    if ([self gameState] == UMGameStarted) {
        [self markLocation:location withPlayer:UMNoneKey];
    } else { 
        if ([self playerTurn] == UMFirstPlayer) {
            [self markLocation:location withPlayer:UMFirstPlayerKey];
        } else {
            [self markLocation:location withPlayer:UMSecondPlayerKey];
        }
    }
}

@synthesize winner = _winner;
- (UMPlayer)winner
{
    NSInteger firstPlayerPoints = [self firstPlayerPoints];
    NSInteger secondPlayerPoints = [self secondPlayerPoints];
    if (firstPlayerPoints > secondPlayerPoints)
        return UMFirstPlayer;
    else if (firstPlayerPoints < secondPlayerPoints)
        return UMSecondPlayer;
    
    return UMNone;
}

@synthesize winnerId = _winnerId;
- (NSString *)winnerId
{
    NSInteger firstPlayerPoints = [self firstPlayerPoints];
    NSInteger secondPlayerPoints = [self secondPlayerPoints];
    
    if (firstPlayerPoints >= secondPlayerPoints)
        return [self firstPlayerId];
    else if (firstPlayerPoints < secondPlayerPoints)
        return [self secondPlayerId];
    
    return nil;
}

- (NSArray *)players
{
    return [[self game] objectForKey:UMPlayersKey];
}

- (NSMutableDictionary *)playerAt:(UMPlayer)player
{
    return [[[self game] objectForKey:UMPlayersKey] objectAtIndex:player];
}

- (void)setId:(NSString *)playerId forPlayerAt:(UMPlayer)player
{
    [[self playerAt:player] setObject:playerId forKey:UMIdKey];
}

- (void)setAlias:(NSString *)alias forPlayerAt:(UMPlayer)player
{
    [[self playerAt:player] setObject:alias forKey:UMAliasKey];
}

- (NSMutableDictionary *)firstPlayer
{
    return [[self players] objectAtIndex:UMFirstPlayer];
}

- (NSMutableDictionary *)secondPlayer
{
    return [[self players] objectAtIndex:UMSecondPlayer];
}

@synthesize firstPlayerId = _firstPlayerId;
- (NSString *)firstPlayerId
{
    return [[self firstPlayer] objectForKey:UMIdKey];
}

@synthesize secondPlayerId = _secondPlayerId;
- (NSString *)secondPlayerId
{
    return [[self secondPlayer] objectForKey:UMIdKey];
}

- (void)setSecondPlayerId:(NSString *)playerId
{
    [[self secondPlayer] setObject:playerId forKey:UMIdKey];
}

@synthesize firstPlayerAlias = _firstPlayerAlias;
- (NSString *)firstPlayerAlias
{
    return [[self firstPlayer] objectForKey:UMAliasKey];
}

@synthesize secondPlayerAlias = _secondPlayerAlias;
- (NSString *)secondPlayerAlias
{
    return [[self secondPlayer] objectForKey:UMAliasKey];
}

- (void)setSecondPlayerAlias:(NSString *)alias
{
    [[self secondPlayer] setObject:alias forKey:UMAliasKey];
}

@synthesize firstPlayerPoints = _firstPlayerPoints;
- (NSInteger)firstPlayerPoints
{
    return [[[self firstPlayer] objectForKey:UMPointsKey] integerValue];
}

@synthesize secondPlayerPoints = _secondPlayerPoints;
- (NSInteger)secondPlayerPoints
{
    return [[[self secondPlayer] objectForKey:UMPointsKey] integerValue];
}

- (NSInteger)pointsForPlayer:(NSDictionary *)player
{
    return [[player objectForKey:UMPointsKey] integerValue];
}

- (void)setPoints:(NSInteger)points forPlayer:(NSMutableDictionary *)player
{
    [player setObject:[NSNumber numberWithInteger:points] forKey:UMPointsKey];
}

@synthesize firstPlayerDirection = _firstPlayerDirection;
- (UMDirection)firstPlayerDirection
{
    return [[[self firstPlayer] objectForKey:UMDirectionKey] shortValue];
}

@synthesize secondPlayerDirection = _secondPlayerDirection;
- (UMDirection)secondPlayerDirection
{
    return [[[self secondPlayer] objectForKey:UMDirectionKey] shortValue];
}

- (UMDirection)directionForPlayerAt:(UMPlayer)player
{
    return [[[[self players] objectAtIndex:player] objectForKey:UMDirectionKey] shortValue];
}

- (void)setDirection:(UMDirection)direction forPlayer:(NSMutableDictionary *)player
{
    [player setObject:[NSNumber numberWithInteger:direction] forKey:UMDirectionKey];
}

@synthesize firstPlayerOutcome = _firstPlayerOutcome;
- (UMOutcome)firstPlayerOutcome
{
    return [[[self firstPlayer] objectForKey:UMOutcomeKey] shortValue];
}

@synthesize secondPlayerOutcome = _secondPlayerOutcome;
- (UMOutcome)secondPlayerOutcome
{
    return [[[self secondPlayer] objectForKey:UMOutcomeKey] shortValue];
}

- (UMOutcome)outcomeForPlayer:(NSDictionary *)player
{
    return [[player objectForKey:UMOutcomeKey] shortValue];
}

- (void)setOutcome:(UMOutcome)outcome forPlayer:(NSMutableDictionary *)player
{
    [player setObject:[NSNumber numberWithInteger:outcome] forKey:UMOutcomeKey];
}

@synthesize mode = _mode;
- (UMMode)mode
{
    return [[[self game] objectForKey:UMModeKey] shortValue];
}

- (void)setMode:(UMMode)mode
{
    [[self game] setObject:[NSNumber numberWithInteger:mode] forKey:UMModeKey];
}

@synthesize board = _board;
- (NSMutableArray *)board
{
    return [[self game] objectForKey:UMBoardKey];
}

- (BOOL)boardContainsNumberOnSquare:(UMSquare)square
{
    NSArray *board = [[self game] objectForKey:UMBoardKey];

    return [[board objectAtIndex:UMSquareToIndex(square)] isKindOfClass:[NSNumber class]];
}

- (NSInteger)boardPointsOnSquare:(UMSquare)square
{
    return [[[self board] objectAtIndex:UMSquareToIndex(square)] integerValue];
}

@synthesize playerTurn = _playerTurn;
- (UMPlayer)playerTurn
{
    return [[[self game] objectForKey:UMPlayerTurnKey] shortValue];
}

- (void)setPlayerTurn:(UMPlayer)playerTurn
{
    [[self game] setObject:[NSNumber numberWithInteger:playerTurn] forKey:UMPlayerTurnKey];
}

@synthesize cursorLocation = _cursorLocation;
- (UMSquare)cursorLocation
{
    UMSquare location = UMIndexToSquare([[[self game] objectForKey:UMCursorLocationKey] integerValue]);
    
    return location;
}

- (void)setCursorLocation:(UMSquare)square
{
    [[self game] setObject:[NSNumber numberWithInteger:UMSquareToIndex(square)] forKey:UMCursorLocationKey];
}

- (void)markLocation: (UMSquare)location withPlayer:(NSString *)player
{
    [[self board] replaceObjectAtIndex:UMSquareToIndex(location) withObject:player];
}

@synthesize gameState = _gameState;
- (UMGameState)gameState
{
    return [[[self game] objectForKey:UMGameStateKey] shortValue];
}

- (void)setGameState:(UMGameState)gameState
{
    [[self game] setObject:[NSNumber numberWithInteger:gameState] forKey:UMGameStateKey];
}

@synthesize score = _score;
- (NSNumber *)score
{
    return [[self game] objectForKey:UMScoreKey];
}

- (void)setScore:(NSNumber *)score
{
    [[self game] setObject:score forKey:UMScoreKey];
}

- (void)reportGameScore
{
    if ([self gameState] != UMGameOver)
        return;
    
    if ([self mode] == UMOnePlayer) {
        [LocalScores store:[[self game] objectForKey:UMScoreKey]];
        [[NSNotificationCenter defaultCenter] postNotificationName:UMGameDidStoreScoreNotification object:self];
    } 
}

- (void)updatePlayer:(UMPlayer)player infoId:(NSString *)iD alias:(NSString *)alias
{
    [self setId:iD ? iD : @"ID-XXXXXXXX" forPlayerAt:player];
    [self setAlias:alias ? alias : @"Me" forPlayerAt:player];
}

- (void)updateLocalPlayerId:(NSString *)iD alias:(NSString *)alias
{
    [self setLocalPlayerId:iD];
    [self setLocalPlayerAlias:alias];
    
    [self updatePlayer:UMFirstPlayer infoId:iD alias:alias];
}

- (void)updatePlayersInfoOnCompletion:(void (^)(void))onCompletion
{
    if ([[self firstPlayerId] isEqualToString:@"ID-XXXXXXXX"] &&
        [[self secondPlayerId] isEqualToString:[self localPlayerId]] == NO) {
        
        [self updatePlayer:UMFirstPlayer infoId:[self localPlayerId] alias:[self localPlayerAlias]];
        [self setPlayerTurn:UMSecondPlayer];
        [self acceptNetworkOpponent];
        onCompletion();
    } else if ([[self secondPlayerId] isEqualToString:@"ID-XXXXXXXX"] &&
               [[self firstPlayerId] isEqualToString:[self localPlayerId]] == NO) {
        
        [self updatePlayer:UMSecondPlayer infoId:[self localPlayerId] alias:[self localPlayerAlias]];
        [self setPlayerTurn:UMFirstPlayer];
        [self acceptNetworkOpponent];
        onCompletion();
    }
}

- (NSMutableDictionary *)localPlayer
{
    if ([[self localPlayerId] isEqualToString:[self firstPlayerId]])
        return [self firstPlayer];
    else if ([[self localPlayerId] isEqualToString:[self secondPlayerId]])
        return [self secondPlayer];
    
    return nil;
}

- (NSMutableDictionary *)opponentPlayer
{
    if ([[self localPlayerId] isEqualToString:[self firstPlayerId]])
        return [self secondPlayer];
    else if ([[self localPlayerId] isEqualToString:[self secondPlayerId]])
        return [self firstPlayer];
    
    return nil;
}

@synthesize isLocalPlayerQuitting = _isLocalPlayerQuitting;
- (BOOL)isLocalPlayerQuitting
{
    return [self outcomeForPlayer:[self localPlayer]] == UMOutcomeQuit;
}

@synthesize isOpponentPlayerQuitting = _isOpponentPlayerQuitting;
- (BOOL)isOpponentPlayerQuitting
{
    return [self outcomeForPlayer:[self opponentPlayer]] == UMOutcomeQuit;
}

@synthesize localPlayerOutcome = _localPlayerOutcome;
- (UMOutcome)localPlayerOutcome
{
    return [self outcomeForPlayer:[self localPlayer]];
}

@synthesize opponentPlayerOutcome = _opponentPlayerOutcome;
- (UMOutcome)opponentPlayerOutcome
{
    return [self outcomeForPlayer:[self opponentPlayer]];
}

//
// MARK: iPhone move
//
- (void)iPhoneMove:(UMMode)mode
{
    switch (mode) {
        case UMOnePlayer:
            [self iPhoneMoveLevel2];
            break;
            
        default:
            break;
    }
    
    [self setPlayerTurn:UMFirstPlayer];
    [self save];
    [self setIsValidMove:YES];
}

- (void)iPhoneMoveLevel1
{
    UMSquare  newLocation = {-1, -1};
    NSInteger max         = -1000;
    UMSquare  location = [self cursorLocation];
    
    if ([self directionForPlayerAt:UMFirstPlayer] == UMHorizontal) {
        
        for (int i = 0; i < UMRows; i++) {
            UMSquare square = {i, location.col};
            if (([self boardContainsNumberOnSquare:square]) &&
                ([self boardPointsOnSquare:square] >= max)) {
                max = [self boardPointsOnSquare:square];
                newLocation = square;
            }
        }
        
    } else {
        
        for (int i = 0; i < UMCols; i++) {
            UMSquare square = {location.row, i};
            if (([self boardContainsNumberOnSquare:square]) &&
                ([self boardPointsOnSquare:square] >= max)) {
                max = [self boardPointsOnSquare:square];
                newLocation = square;
            }
        }
        
    }
    
    [self addToPlayer:UMSecondPlayer pointsFromSquare:newLocation];
    [self setCursorLocation:newLocation];
    [self updateBoardInLocation:newLocation];
}

- (void)iPhoneMoveLevel2
{
    if ([self directionForPlayerAt:UMFirstPlayer] == UMHorizontal) {
        [self iPhoneMoveLevel2Vertically];
    } else {
        [self iPhoneMoveLevel2Horizontally];
    }
}

- (void)iPhoneMoveLevel2Horizontally
{
    UMSquare iPhoneBS = {-1, -1};
    int iPhoneBV = -2000;
    
    NSArray *board = [self board];
    NSInteger firstPlayerPoints = [self firstPlayerPoints];
    NSInteger secondPlayerPoints = [self secondPlayerPoints];
    UMSquare location= [self cursorLocation];
    
    /*
     * iPhone is moving horizontally. Now we need to go through 
     * each iPhone possible move.
     */
    for (int i = 0; i < UMCols; i++) {
        /*
         * Check if iPhone can make a move.
         */
        UMSquare iPhoneSquare = {location.row, i};
        if ([[board objectAtIndex:UMSquareToIndex(iPhoneSquare)] isKindOfClass:[NSNumber class]] == NO)
            continue;
        
        /*
         * Now we have a possible best iPhone move. We temporaly store its 
         * content value. 
         */
        int iPhoneBPV = [[board objectAtIndex:UMSquareToIndex(iPhoneSquare)] intValue];
        
        /*
         * Let's assume that the player will choose the largest value of the row
         * "chosen" by the iPhone.
         */
        int myBPV = -1000;
        for (int j = 0; j < UMRows; j++) {
            UMSquare mySquare = {j, iPhoneSquare.col};
            if ([[board objectAtIndex:UMSquareToIndex(mySquare)] isKindOfClass:[NSNumber class]] == NO)
                continue;
            
            if (myBPV < [[board objectAtIndex:UMSquareToIndex(mySquare)] intValue])
                myBPV = [[board objectAtIndex:UMSquareToIndex(mySquare)] intValue];
        }
        
        /*
         * If the player does not have a position available and 
         * the iPhone would win, then we move and we are done.
         */
        if (myBPV == -1000) {
            if (iPhoneBPV + secondPlayerPoints > firstPlayerPoints) {
                [self addToPlayer:UMSecondPlayer pointsFromSquare:iPhoneSquare];
                [self setCursorLocation:iPhoneSquare];
                [self updateBoardInLocation:iPhoneSquare]; 
                
                return;
            } else {
                iPhoneBV = iPhoneBPV;
                iPhoneBS = iPhoneSquare;
            }
        } else if (iPhoneBV < (iPhoneBPV - myBPV)) {
            iPhoneBV = (iPhoneBPV - myBPV);
            iPhoneBS = iPhoneSquare;
        }
    }
    
    if (iPhoneBS.row != -1 && iPhoneBS.col != -1) {
        [self addToPlayer:UMSecondPlayer pointsFromSquare:iPhoneBS];
        [self setCursorLocation:iPhoneBS];
        [self updateBoardInLocation:iPhoneBS];
    } else {
        DDLogError(@"Error en AI");
    }        
}


- (void)iPhoneMoveLevel2Vertically
{
    UMSquare iPhoneBS = {-1, -1};
    int iPhoneBV = -2000;
    
    NSArray *board = [self board];
    NSInteger firstPlayerPoints = [self firstPlayerPoints];
    NSInteger secondPlayerPoints = [self secondPlayerPoints];
    UMSquare location = [self cursorLocation];
    
    /*
     * iPhone is moving vertically. Now we need to go through 
     * each iPhone possible move.
     */
    for (int i = 0; i < UMRows; i++) {
        /*
         * Check if iPhone can make a move.
         */
        UMSquare iPhoneSquare = {i, location.col};
        if ([[board objectAtIndex:UMSquareToIndex(iPhoneSquare)] isKindOfClass:[NSNumber class]] == NO)
            continue;
        
        /*
         * Now we have a possible best iPhone move. We temporaly store its 
         * content value. 
         */
        int iPhoneBPV = [[board objectAtIndex:UMSquareToIndex(iPhoneSquare)] intValue];
        
        /*
         * Let's assume that the player will choose the largest value of the row
         * "chosen" by the iPhone.
         */
        int myBPV = -1000;
        for (int j = 0; j < UMCols; j++) {
            UMSquare mySquare = {iPhoneSquare.row, j};
            if ([[board objectAtIndex:UMSquareToIndex(mySquare)] isKindOfClass:[NSNumber class]] == NO)
                continue;
            
            if (myBPV < [[board objectAtIndex:UMSquareToIndex(mySquare)] intValue])
                myBPV = [[board objectAtIndex:UMSquareToIndex(mySquare)] intValue];
        }
        
        /*
         * If the player does not have a position available and 
         * the iPhone would win, then we move and we are done.
         */
        if (myBPV == -1000) {
            if (iPhoneBPV + secondPlayerPoints > firstPlayerPoints) {
                [self addToPlayer:UMSecondPlayer pointsFromSquare:iPhoneSquare];
                [[self game] setObject:[NSNumber numberWithInteger:UMSquareToIndex(iPhoneSquare)] forKey:UMCursorLocationKey];
                [self updateBoardInLocation:iPhoneSquare]; 
                
                return;
            } else {
                iPhoneBV = iPhoneBPV;
                iPhoneBS = iPhoneSquare;
            }
        } else if (iPhoneBV < (iPhoneBPV - myBPV)) {
            iPhoneBV = (iPhoneBPV - myBPV);
            iPhoneBS = iPhoneSquare;
        }
    }
    
    if (iPhoneBS.row != -1 && iPhoneBS.col != -1) {
        [self addToPlayer:UMSecondPlayer pointsFromSquare:iPhoneBS];
        [[self game] setObject:[NSNumber numberWithInteger:UMSquareToIndex(iPhoneBS)] forKey:UMCursorLocationKey];
        [self updateBoardInLocation:iPhoneBS];
    } else {
        DDLogError(@"Error en AI");
    }    
}

//
// MARK: save, restore, and stores
//
- (void)save
{
}

- (void)restore
{
}

//
// MARK: New game methods
//
- (void)newGameWithMode:(UMMode)mode
{
    [self reset];
    
    switch (mode) {
        case UMOnePlayer:
            [self setMode:UMOnePlayer];
            [self setPlayerTurn:UMFirstPlayer];
            [self setGameState:UMGameStarted];
            break;
            
        case UMTwoPlayers:
            [self setMode:UMTwoPlayers];
            [self setPlayerTurn:UMFirstPlayer];
            [self setGameState:UMGameStarted];
            break;
            
        case UMNetwork:
            [self setMode:UMNetwork];
            [self setPlayerTurn:UMSecondPlayer];
            [self setGameState:UMGameIdle];
            break;
            
        default:
            break;
    }
    
    [self updatePlayer:UMFirstPlayer infoId:[self localPlayerId] alias:[self localPlayerAlias]];
}

- (void)reset
{
    NSMutableDictionary *firstPlayer = [[NSMutableDictionary alloc] init];
    [firstPlayer setObject:@"ID-XXXXXXXX" forKey:UMIdKey];
    [firstPlayer setObject:@"" forKey:UMAliasKey];
    [firstPlayer setObject:[NSNumber numberWithInteger:0] forKey:UMPointsKey];
    [firstPlayer setObject:[NSNumber numberWithInteger:UMHorizontal] forKey:UMDirectionKey];
    [firstPlayer setObject:[NSNumber numberWithInteger:UMOutcomeNone] forKey:UMOutcomeKey];
    
    NSMutableDictionary *secondPlayer = [[NSMutableDictionary alloc] init];
    [secondPlayer setObject:@"ID-XXXXXXXX" forKey:UMIdKey];
    [secondPlayer setObject:@"" forKey:UMAliasKey];
    [secondPlayer setObject:[NSNumber numberWithInteger:0] forKey:UMPointsKey];
    [secondPlayer setObject:[NSNumber numberWithInteger:UMVertical] forKey:UMDirectionKey];
    [secondPlayer setObject:[NSNumber numberWithInteger:UMOutcomeNone] forKey:UMOutcomeKey];
    
    NSMutableArray *players = [[NSMutableArray alloc] initWithObjects:firstPlayer, secondPlayer, nil];
    
    [self setGame:[[NSMutableDictionary alloc] init]];
    [[self game] setObject:players forKey:UMPlayersKey];
    [[self game] setObject:[NSNumber numberWithInteger:0] forKey:UMScoreKey];
    
    int initialRow = (int)((double)rand() / ((double)RAND_MAX + 1) * UMRows);
    int initialCol = (int)((double)rand() / ((double)RAND_MAX + 1) * UMCols);
    UMSquare cursor = {initialRow, initialCol};
    [[self game] setObject:[NSNumber numberWithInteger:UMSquareToIndex(cursor)] forKey:UMCursorLocationKey];
    
    NSMutableArray *board = [NSMutableArray arrayWithArray:[self fillSquares]];
    [board replaceObjectAtIndex:UMSquareToIndex(cursor) withObject:UMNoneKey];
    [[self game] setObject:board forKey:UMBoardKey];
    
    [[self game] setObject:[NSNumber numberWithInteger:UMFirstPlayer] forKey:UMPlayerTurnKey];
    [[self game] setObject:[NSNumber numberWithInteger:UMOnePlayer] forKey:UMModeKey];
    [[self game] setObject:[NSNumber numberWithInteger:UMGameStarted] forKey:UMGameStateKey];
    [[self game] setObject:[NSDate date] forKey:UMDateKey];
}

- (void)acceptNetworkOpponent
{
    if ([self gameState] == UMGameIdle)
        [self setGameState:UMGameStarted];
}

- (void)endGameLocalPlayerOutcome:(UMOutcome)localOutcome opponentPlayerOutcome:(UMOutcome)opponentOutcome
{
    [self setOutcome:localOutcome forPlayer:[self localPlayer]];
    [self setOutcome:opponentOutcome forPlayer:[self opponentPlayer]];
    [self setGameState:UMGameOver];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:UMGameDidUpdateNotification object:self];
}

//
// MARK: Initializers
//
- (id)initWithMode:(UMMode)mode
{
    self = [super init];
    if (!self)
        return nil;
    
    [self newGameWithMode:mode];
    
    return self;
}

@end
